module.exports = function (api) {
    api.cache(true);

    return {
        plugins: [
            '@babel/plugin-proposal-class-properties'
            // '@babel/plugin-proposal-object-rest-spread',
            // 'babel-plugin-transform-async-to-promises'
        ],
        ignore: ['./dist']
    };
};