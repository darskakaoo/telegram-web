import { config } from '../telegram-api/telegram';
import { render } from '../engine/template';

export function loginView() {
    return render('auth/login', config);
}

export function codeView({ state }) {
    return render('auth/code', state);
}

export function passwordView({ state }) {
    return render('auth/password', state);
}

export function signupView({ state }) {
    return render('auth/signup', state);
}