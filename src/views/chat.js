import { render } from '../engine/template';

export function chatListView(_1, _2, { peerId = -1, state: { messages: { pinned = [], unpinned = [] } = { pinned: [], unpinned: [] } } }) {
    const showLoading = pinned.length === 0 && unpinned.length === 0;

    return render('chat/dialog/chatList', {
        showLoading,
        pinned,
        unpinned,
        activePeerId: peerId
    });
}