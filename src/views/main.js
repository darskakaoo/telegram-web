import localForage from '../utils/storage';
import { navigate } from '../engine/router';

export async function mainView() {
    const selfId = await localForage.get('self-id');

    if (selfId) {
        // there's an auth key registered in the storage, assume the user has logged in
        navigate('/messages/');
    } else {
        navigate('/login/');
    }
}