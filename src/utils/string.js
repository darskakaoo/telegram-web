String.prototype.asyncReplace = async function (pattern, replace) {
    const promises = [];
    const value = this.valueOf();
    value.replace(pattern, (...args) => {
        const promise = replace(...args);
        promises.push(promise);

        return args[1];
    });

    const data = await Promise.all(promises);

    return value.replace(pattern, () => data.shift() || '');
};

const defaultSubstring = String.prototype.substring;

String.prototype.substring = function (start, end) {
    const value = this.valueOf();

    if (end && end < 0) {
        end += value.length;
    }
    
    return defaultSubstring.call(value, start, end);
};

export { };
