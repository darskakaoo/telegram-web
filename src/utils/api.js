import * as functions from '../telegram-api';

export function callAPI(parts, values, next, error = console.error) {
    if (typeof (parts) === 'string') {
        parts = parts.split('.');
    }

    let apiFunction = functions;
    for (const partName of parts) {
        apiFunction = apiFunction[partName];
    }
    
    apiFunction(values)
    .then(next)
    .catch(error);
};

export function bindForm(form, next, error, beforeSubmit) {

    form.addEventListener('submit', function (event) {
        event.preventDefault();

        const childInputs = this.querySelectorAll('input');
        let values = {};
        childInputs.forEach(({ name, value }) => values[name] = value);

        if (beforeSubmit) {
            values = beforeSubmit(values);
        }

        callAPI(this.classList, values, next, error)
    });
};

export function generateRandomId() {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;

    for (let i = 0; i < 60; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

window.generateRandomId = generateRandomId;
window.callAPI = callAPI;
window.bindForm = bindForm;