import localForage from 'localforage';

const functionsToChange = ['set', 'get', 'remove'];
for (const functionName of functionsToChange) {
    localForage[functionName] = localForage[`${functionName}Item`];
}

window.storage = localForage;
export default localForage;