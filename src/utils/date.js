function padNumber(number) {
    if (number < 10) {
        return `0${number}`;
    } else {
        return number.toString();
    }
}

const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export default function formatDate(date, format) {
    const hour = date.getHours();
    const paddedHour = padNumber(hour);

    const minute = date.getMinutes();
    const paddedMinute = padNumber(minute);

    const dayName = dayNames[date.getDay()];

    const day = date.getDate();
    const paddedDay = padNumber(day);

    const month = date.getMonth();
    const paddedMonth = padNumber(month);

    const year = date.getFullYear() % 100;
    const paddedYear = padNumber(year);

    return format
        .replace(/HH/, paddedHour)
        .replace(/MM/, paddedMinute)
        .replace(/D/, dayName)
        .replace(/dd/, paddedDay)
        .replace(/mm/, paddedMonth)
        .replace(/yy/, paddedYear);
}