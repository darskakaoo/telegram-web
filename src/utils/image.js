function _arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

export function bytesToImage(bytes) {
    return `data:image/png;base64,${_arrayBufferToBase64(bytes)}`;
}

window.bytesToImage = bytesToImage;