class TGIcon extends HTMLElement {
    static name = 'tg-icon';

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const styleElement = document.createElement('link');
        styleElement.setAttribute('rel', 'stylesheet');
        styleElement.setAttribute('href', '/templates/components/icon/icon.css');

        shadow.appendChild(styleElement);

        const iconName = this.getAttribute('name');
        const spanElement = document.createElement('span');
        spanElement.classList.add(TGIcon.buildClassName(iconName));

        if (this.getAttribute('align') === 'center') {
            spanElement.classList.add('center-aligned');
        }

        shadow.appendChild(spanElement);
    }

    static buildClassName(name) {
        return `icon-${name}`;
    }

    static get observedAttributes() {
        return ['name', 'align'];
    }

    attributeChangedCallback(attrName, oldValue, newValue) {

        const spanElement = this.shadowRoot.querySelector('span');
        if (attrName === 'name') {
            spanElement.classList.remove(TGIcon.buildClassName(oldValue));
            spanElement.classList.add(TGIcon.buildClassName(newValue));
        } else if (attrName === 'align') {
            if (newValue === 'center') {
                spanElement.classList.add('center-aligned');
            } else {
                spanElement.classList.remove('center-aligned');
            }
        }
    }
}

if (!customElements.get(TGIcon.name)) {
    customElements.define(TGIcon.name, TGIcon);
}