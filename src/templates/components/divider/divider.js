class TGDivider extends HTMLElement {
    static name = 'tg-divider';

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const styleLink = document.createElement('link');
        styleLink.setAttribute('rel', 'stylesheet');
        styleLink.setAttribute('href', '/templates/components/divider/divider.css');

        shadow.appendChild(styleLink);

        const childDiv = document.createElement('div');
        childDiv.classList.add('divider');

        shadow.appendChild(childDiv);
    }
}

if (!customElements.get(TGDivider.name)) {
    customElements.define(TGDivider.name, TGDivider);
}