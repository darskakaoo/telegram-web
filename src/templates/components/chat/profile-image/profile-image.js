class TGProfileImage extends HTMLElement {
    static name = 'tg-profile-image';
    static colors = ['red', 'orange', 'violet', 'green', 'cyan', 'blue', 'pink'];

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const styleElement = document.createElement('link');
        styleElement.setAttribute('rel', 'stylesheet');
        styleElement.setAttribute('href', '/templates/components/chat/profile-image/profile-image.css');
        shadow.appendChild(styleElement);

        const typographyStyleElement = document.createElement('link');
        typographyStyleElement.setAttribute('rel', 'stylesheet');
        typographyStyleElement.setAttribute('href', '/templates/components/typography/typography.css');
        shadow.appendChild(typographyStyleElement);

        let fullName = this.dataset.name;

        if (fullName === 'Saved Messages') {
            const icon = document.createElement('tg-icon');
            icon.setAttribute('name', 'avatar_savedmessages');
            icon.setAttribute('align', 'center');

            const parentDiv = document.createElement('div');
            parentDiv.classList.add('profile', 'image', 'accent');
            parentDiv.appendChild(icon);

            shadow.appendChild(parentDiv);
        } else {

            let photo = this.getAttribute('smallPhoto');

            if (photo !== null) {
                const { local_id, volume_id } = JSON.parse(photo.replace(/\'/g, '"'));
                const peerId = this.getAttribute('peerId');
                const peerType = this.getAttribute('peerType');

                const peer = createPeer(peerType, peerId, this.dataset.accessHash);

                const smallPhotoKey = `photo-${peerId}-small`;

                storage.get(smallPhotoKey).then(result => {
                    if (result) {
                        this.setImage(result);
                    } else {
                        callAPI('upload.getUserPhoto', {
                            peer,
                            local_id,
                            volume_id
                        }, (imageBytes) => {
                            result = bytesToImage(imageBytes);
                            storage.set(smallPhotoKey, result);
                            this.setImage(result);
                        });
                    }
                });
            }


            const nameParts = fullName.split(' ');
            let name = nameParts[0][0].toUpperCase();
            this.name = name;
            if (nameParts.length > 1) {
                name += nameParts[nameParts.length - 1][0].toUpperCase();
            }
            const defaultImageElement = document.createElement('div');
            defaultImageElement.classList.add('profile', 'image', 'none', TGProfileImage.randomColor);

            defaultImageElement.innerHTML = `
            <div class="typography h4 white medium">
                ${name}
            </div>
            `;

            shadow.appendChild(defaultImageElement);
        }
    }

    setImage(base64) {
        const oldImage = this.shadowRoot.querySelector('.profile.image');
        oldImage.remove();

        const newImage = document.createElement('img');
        newImage.classList.add('profile', 'image');
        newImage.setAttribute('alt', this.name);
        newImage.src = base64;

        this.shadowRoot.appendChild(newImage);
    }

    static get randomColor() {
        const randomIndex = Math.floor(Math.random() * TGProfileImage.colors.length);

        return TGProfileImage.colors[randomIndex];
    }
}

if (!customElements.get(TGProfileImage.name)) {
    customElements.define(TGProfileImage.name, TGProfileImage);
}