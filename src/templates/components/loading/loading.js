class TGLoading extends HTMLElement {
    static name = 'tg-loading';

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const styleElement = document.createElement('link');
        styleElement.setAttribute('rel', 'stylesheet');
        styleElement.setAttribute('href', '/templates/components/loading/loading.css');

        shadow.appendChild(styleElement);

        const divElement = document.createElement('div');
        divElement.classList.add('tg-loading-rolling');
        const innerDivElement = document.createElement('div');

        divElement.appendChild(innerDivElement);

        shadow.appendChild(divElement);
    }

    static get observedAttributes() {
        return ['color', 'size'];
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
        if (attrName === 'color') {
            const innerDivElement = this.shadowRoot.querySelector('div div');
            innerDivElement.classList.remove(oldValue);
            innerDivElement.classList.add(newValue);
        }
        if (attrName === 'size') {
            const innerDivElement = this.shadowRoot.querySelector('div div');
            innerDivElement.classList.remove(oldValue);
            innerDivElement.classList.add(newValue);
        }
    }
}

if (!customElements.get(TGLoading.name)) {
    customElements.define(TGLoading.name, TGLoading);
}
