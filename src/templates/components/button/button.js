class TGBUtton extends HTMLButtonElement {
    constructor() {
        super();

        this.addEventListener('mousedown', (event) => {
            const { target, layerX, layerY } = event;
            const ripple = document.createElement('div');
            ripple.classList.add('ripple');
            target.appendChild(ripple);
            ripple.classList.add('active');
            ripple.style = `top: ${layerY}px;left: ${layerX}px;`;

            setTimeout(() => {
                ripple.remove();
            }, 1000);
        });
    }
}

if (!customElements.get('tg-button')) {
    customElements.define('tg-button', TGBUtton, { extends: 'button' });
}