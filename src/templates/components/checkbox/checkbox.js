class TGCheckbox extends HTMLElement {
    static name = 'tg-checkbox';

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        const styleElement = document.createElement('link');
        styleElement.setAttribute('rel', 'stylesheet');
        styleElement.setAttribute('href', '/templates/components/checkbox/checkbox.css');

        shadow.appendChild(styleElement);

        const divElement = document.createElement('div');
        divElement.classList.add('tg-checkbox-wrapper');
        const inputElement = document.createElement('input');
        inputElement.type = 'checkbox';
        inputElement.checked = true;
        inputElement.classList.add('tg-checkbox');
        const backgroundElement = document.createElement('div');
        backgroundElement.classList.add('tg-checkbox-background');

        const tgIcon = document.createElement('tg-icon');
        tgIcon.setAttribute('name', 'check');

        divElement.appendChild(inputElement);
        divElement.appendChild(tgIcon);
        divElement.appendChild(backgroundElement);

        shadow.appendChild(divElement);
    }

    static get observedAttributes() {
        return ['id', 'name'];
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
        if (attrName === 'id') {
            const inputElement = this.shadowRoot.querySelector('input');
            inputElement.setAttribute('id', newValue);
        }
        if (attrName === 'name') {
            const inputElement = this.shadowRoot.querySelector('input');
            inputElement.setAttribute('name', newValue);
        }
    }
}

if (!customElements.get('tg-checkbox')) {
    customElements.define(TGCheckbox.name, TGCheckbox);
}
