class TGPadding extends HTMLElement {
    static name = 'tg-padding';

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        let element = 'div';
        if (this.hasAttribute('inline')) {
            element = 'span';
        }
        const wrapperDiv = document.createElement(element);
        wrapperDiv.style.padding = this.getAttribute('padding');

        shadow.appendChild(wrapperDiv);
    }
}

if (!customElements.get(TGPadding.name)) {
    customElements.define(TGPadding.name, TGPadding);
}
