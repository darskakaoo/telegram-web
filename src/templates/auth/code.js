(
    function () {
        const form = document.querySelector('form');
        const codeInput = document.getElementById('code');
        const monkeyPlayer = document.querySelector('tgs-player');

        let monkeyState = 'idle';
        let previousLength = 0;
        let startFrame = 0;
        let endFrame = 15;

        function handleChange() {
            const length = this.value.trim().length;

            handleMonkey(length);
            previousLength = length;

            if (length > 0 && this.classList.contains('wrong')) {
                this.classList.remove('wrong');
                codeInput.parentElement.querySelector('label').innerHTML = 'Code';
            } else if (length === 0) {
                this.classList.add('wrong');
                codeInput.parentElement.querySelector('label').innerHTML = 'Invalid Code';
            }

            if (length === 5) {
                form.dispatchEvent(new Event('submit'));
            }
        }

        function frameEventListner({ detail: { frame } }) {
            if (Math.abs(frame - endFrame) < 0.1) {
                monkeyPlayer.pause();
            }
        }

        function setIdleMonkey() {
            monkeyState = 'idle';
            monkeyPlayer.load('/animations/TwoFactorSetupMonkeyIdle.tgs');
            monkeyPlayer.loop = true;
            monkeyPlayer.autoplay = true;
            monkeyPlayer.removeEventListener('frame', frameEventListner);
            monkeyPlayer.play();
        }

        function setTrackingMonkey() {
            monkeyState = 'tracking';
            monkeyPlayer.load('/animations/TwoFactorSetupMonkeyTracking.tgs');
            monkeyPlayer.loop = false;
            monkeyPlayer.getLottie().goToAndStop(endFrame, true);

            monkeyPlayer.addEventListener('frame', frameEventListner)
        }

        function handleMonkey(length) {

            if (monkeyState === 'idle' && length > 0) {
                setTrackingMonkey();
            }

            if (monkeyState === 'tracking') {
                startFrame = previousLength * 5 + 15;
                endFrame = length * 5 + 15;
                if (startFrame < endFrame) {
                    monkeyPlayer.setDirection(1);
                } else {
                    monkeyPlayer.setDirection(-1);
                }
                monkeyPlayer.play();
            }
        }

        codeInput.addEventListener('input', handleChange);
        codeInput.addEventListener('focus', setTrackingMonkey);
        codeInput.addEventListener('blur', setIdleMonkey);


        bindForm(form, ({ nextStep }) => {
            if (nextStep === 'messages') {
                navigate('/messages/');
            } else {
                navigate('/signup/', history.state);
            }
        }, (error) => {
            switch (error.type) {
                case 'PHONE_CODE_EMPTY':
                case 'PHONE_CODE_INVALID': {
                    codeInput.classList.add('wrong');
                    codeInput.parentElement.querySelector('label').innerHTML = 'Invalid Code';
                    break;
                }
                case 'PHONE_CODE_EXPIRED': {
                    // TODO
                    break;
                }
                case 'SESSION_PASSWORD_NEEDED': {
                    navigate('/login/password/');
                    break;
                }

            }
        });
    }
)();