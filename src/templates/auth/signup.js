(
    function () {
        const form = document.querySelector('form');
        const firstNameInput = document.getElementById('first_name');
        const submitButton = form.querySelector('button[type="submit"]');

        function handleChange() {

            if (!firstNameInput.value || firstNameInput.value.trim().length === 0) {
                submitButton.classList.add('invisible');
            } else {
                submitButton.classList.remove('invisible');
            }
        }

        firstNameInput.addEventListener('input', handleChange);

        bindForm(form, ({ }) => {
            navigate('/messages/');
        }, console.error, (values) => {
            return {
                ...values,
                first_name: values.first_name.trim()
            };
        });
    }
)();