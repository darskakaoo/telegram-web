window.countryList = [{ "iso3": "AFG", "name": "Afghanistan", "flag": "🇦🇫", "code": ["+93"], "search": "AFAFGAfghanistan+93", "key": "AF" }, { "iso3": "ALA", "name": "Aland Islands", "flag": "🇦🇽", "code": ["+358-18"], "search": "AXALAAland Islands+358-18", "key": "AX" }, { "iso3": "ALB", "name": "Albania", "flag": "🇦🇱", "code": ["+355"], "search": "ALALBAlbania+355", "key": "AL" }, { "iso3": "DZA", "name": "Algeria", "flag": "🇩🇿", "code": ["+213"], "search": "DZDZAAlgeria+213", "key": "DZ" }, { "iso3": "ASM", "name": "American Samoa", "flag": "🇦🇸", "code": ["+1-684"], "search": "ASASMAmerican Samoa+1-684", "key": "AS" }, { "iso3": "AND", "name": "Andorra", "flag": "🇦🇩", "code": ["+376"], "search": "ADANDAndorra+376", "key": "AD" }, { "iso3": "AGO", "name": "Angola", "flag": "🇦🇴", "code": ["+244"], "search": "AOAGOAngola+244", "key": "AO" }, { "iso3": "AIA", "name": "Anguilla", "flag": "🇦🇮", "code": ["+1-264"], "search": "AIAIAAnguilla+1-264", "key": "AI" }, { "iso3": "ATG", "name": "Antigua and Barbuda", "flag": "🇦🇬", "code": ["+1-268"], "search": "AGATGAntigua and Barbuda+1-268", "key": "AG" }, { "iso3": "ARG", "name": "Argentina", "flag": "🇦🇷", "code": ["+54"], "search": "ARARGArgentina+54", "key": "AR" }, { "iso3": "ARM", "name": "Armenia", "flag": "🇦🇲", "code": ["+374"], "search": "AMARMArmenia+374", "key": "AM" }, { "iso3": "ABW", "name": "Aruba", "flag": "🇦🇼", "code": ["+297"], "search": "AWABWAruba+297", "key": "AW" }, { "iso3": "AUS", "name": "Australia", "flag": "🇦🇺", "code": ["+61"], "search": "AUAUSAustralia+61", "key": "AU" }, { "iso3": "AUT", "name": "Austria", "flag": "🇦🇹", "code": ["+43"], "search": "ATAUTAustria+43", "key": "AT" }, { "iso3": "AZE", "name": "Azerbaijan", "flag": "🇦🇿", "code": ["+994"], "search": "AZAZEAzerbaijan+994", "key": "AZ" }, { "iso3": "BHS", "name": "Bahamas", "flag": "🇧🇸", "code": ["+1-242"], "search": "BSBHSBahamas+1-242", "key": "BS" }, { "iso3": "BHR", "name": "Bahrain", "flag": "🇧🇭", "code": ["+973"], "search": "BHBHRBahrain+973", "key": "BH" }, { "iso3": "BGD", "name": "Bangladesh", "flag": "🇧🇩", "code": ["+880"], "search": "BDBGDBangladesh+880", "key": "BD" }, { "iso3": "BRB", "name": "Barbados", "flag": "🇧🇧", "code": ["+1-246"], "search": "BBBRBBarbados+1-246", "key": "BB" }, { "iso3": "BLR", "name": "Belarus", "flag": "🇧🇾", "code": ["+375"], "search": "BYBLRBelarus+375", "key": "BY" }, { "iso3": "BEL", "name": "Belgium", "flag": "🇧🇪", "code": ["+32"], "search": "BEBELBelgium+32", "key": "BE" }, { "iso3": "BLZ", "name": "Belize", "flag": "🇧🇿", "code": ["+501"], "search": "BZBLZBelize+501", "key": "BZ" }, { "iso3": "BEN", "name": "Benin", "flag": "🇧🇯", "code": ["+229"], "search": "BJBENBenin+229", "key": "BJ" }, { "iso3": "BMU", "name": "Bermuda", "flag": "🇧🇲", "code": ["+1-441"], "search": "BMBMUBermuda+1-441", "key": "BM" }, { "iso3": "BTN", "name": "Bhutan", "flag": "🇧🇹", "code": ["+975"], "search": "BTBTNBhutan+975", "key": "BT" }, { "iso3": "BOL", "name": "Bolivia", "flag": "🇧🇴", "code": ["+591"], "search": "BOBOLBolivia+591", "key": "BO" }, { "iso3": "BES", "name": "Bonaire, Saint Eustatius and Saba ", "flag": "🇧🇶", "code": ["+599"], "search": "BQBESBonaire, Saint Eustatius and Saba +599", "key": "BQ" }, { "iso3": "BIH", "name": "Bosnia and Herzegovina", "flag": "🇧🇦", "code": ["+387"], "search": "BABIHBosnia and Herzegovina+387", "key": "BA" }, { "iso3": "BWA", "name": "Botswana", "flag": "🇧🇼", "code": ["+267"], "search": "BWBWABotswana+267", "key": "BW" }, { "iso3": "BRA", "name": "Brazil", "flag": "🇧🇷", "code": ["+55"], "search": "BRBRABrazil+55", "key": "BR" }, { "iso3": "IOT", "name": "British Indian Ocean Territory", "flag": "🇮🇴", "code": ["+246"], "search": "IOIOTBritish Indian Ocean Territory+246", "key": "IO" }, { "iso3": "VGB", "name": "British Virgin Islands", "flag": "🇻🇬", "code": ["+1-284"], "search": "VGVGBBritish Virgin Islands+1-284", "key": "VG" }, { "iso3": "BRN", "name": "Brunei", "flag": "🇧🇳", "code": ["+673"], "search": "BNBRNBrunei+673", "key": "BN" }, { "iso3": "BGR", "name": "Bulgaria", "flag": "🇧🇬", "code": ["+359"], "search": "BGBGRBulgaria+359", "key": "BG" }, { "iso3": "BFA", "name": "Burkina Faso", "flag": "🇧🇫", "code": ["+226"], "search": "BFBFABurkina Faso+226", "key": "BF" }, { "iso3": "BDI", "name": "Burundi", "flag": "🇧🇮", "code": ["+257"], "search": "BIBDIBurundi+257", "key": "BI" }, { "iso3": "KHM", "name": "Cambodia", "flag": "🇰🇭", "code": ["+855"], "search": "KHKHMCambodia+855", "key": "KH" }, { "iso3": "CMR", "name": "Cameroon", "flag": "🇨🇲", "code": ["+237"], "search": "CMCMRCameroon+237", "key": "CM" }, { "iso3": "CAN", "name": "Canada", "flag": "🇨🇦", "code": ["+1"], "search": "CACANCanada+1", "key": "CA" }, { "iso3": "CPV", "name": "Cape Verde", "flag": "🇨🇻", "code": ["+238"], "search": "CVCPVCape Verde+238", "key": "CV" }, { "iso3": "CYM", "name": "Cayman Islands", "flag": "🇰🇾", "code": ["+1-345"], "search": "KYCYMCayman Islands+1-345", "key": "KY" }, { "iso3": "CAF", "name": "Central African Republic", "flag": "🇨🇫", "code": ["+236"], "search": "CFCAFCentral African Republic+236", "key": "CF" }, { "iso3": "TCD", "name": "Chad", "flag": "🇹🇩", "code": ["+235"], "search": "TDTCDChad+235", "key": "TD" }, { "iso3": "CHL", "name": "Chile", "flag": "🇨🇱", "code": ["+56"], "search": "CLCHLChile+56", "key": "CL" }, { "iso3": "CHN", "name": "China", "flag": "🇨🇳", "code": ["+86"], "search": "CNCHNChina+86", "key": "CN" }, { "iso3": "CXR", "name": "Christmas Island", "flag": "🇨🇽", "code": ["+61"], "search": "CXCXRChristmas Island+61", "key": "CX" }, { "iso3": "CCK", "name": "Cocos Islands", "flag": "🇨🇨", "code": ["+61"], "search": "CCCCKCocos Islands+61", "key": "CC" }, { "iso3": "COL", "name": "Colombia", "flag": "🇨🇴", "code": ["+57"], "search": "COCOLColombia+57", "key": "CO" }, { "iso3": "COM", "name": "Comoros", "flag": "🇰🇲", "code": ["+269"], "search": "KMCOMComoros+269", "key": "KM" }, { "iso3": "COK", "name": "Cook Islands", "flag": "🇨🇰", "code": ["+682"], "search": "CKCOKCook Islands+682", "key": "CK" }, { "iso3": "CRI", "name": "Costa Rica", "flag": "🇨🇷", "code": ["+506"], "search": "CRCRICosta Rica+506", "key": "CR" }, { "iso3": "HRV", "name": "Croatia", "flag": "🇭🇷", "code": ["+385"], "search": "HRHRVCroatia+385", "key": "HR" }, { "iso3": "CUB", "name": "Cuba", "flag": "🇨🇺", "code": ["+53"], "search": "CUCUBCuba+53", "key": "CU" }, { "iso3": "CUW", "name": "Curacao", "flag": "🇨🇼", "code": ["+599"], "search": "CWCUWCuracao+599", "key": "CW" }, { "iso3": "CYP", "name": "Cyprus", "flag": "🇨🇾", "code": ["+357"], "search": "CYCYPCyprus+357", "key": "CY" }, { "iso3": "CZE", "name": "Czech Republic", "flag": "🇨🇿", "code": ["+420"], "search": "CZCZECzech Republic+420", "key": "CZ" }, { "iso3": "COD", "name": "Democratic Republic of the Congo", "flag": "🇨🇩", "code": ["+243"], "search": "CDCODDemocratic Republic of the Congo+243", "key": "CD" }, { "iso3": "DNK", "name": "Denmark", "flag": "🇩🇰", "code": ["+45"], "search": "DKDNKDenmark+45", "key": "DK" }, { "iso3": "DJI", "name": "Djibouti", "flag": "🇩🇯", "code": ["+253"], "search": "DJDJIDjibouti+253", "key": "DJ" }, { "iso3": "DMA", "name": "Dominica", "flag": "🇩🇲", "code": ["+1-767"], "search": "DMDMADominica+1-767", "key": "DM" }, { "iso3": "DOM", "name": "Dominican Republic", "flag": "🇩🇴", "code": ["+1-809", "+ 1-829"], "search": "DODOMDominican Republic+1-809,+ 1-829", "key": "DO" }, { "iso3": "TLS", "name": "East Timor", "flag": "🇹🇱", "code": ["+670"], "search": "TLTLSEast Timor+670", "key": "TL" }, { "iso3": "ECU", "name": "Ecuador", "flag": "🇪🇨", "code": ["+593"], "search": "ECECUEcuador+593", "key": "EC" }, { "iso3": "EGY", "name": "Egypt", "flag": "🇪🇬", "code": ["+20"], "search": "EGEGYEgypt+20", "key": "EG" }, { "iso3": "SLV", "name": "El Salvador", "flag": "🇸🇻", "code": ["+503"], "search": "SVSLVEl Salvador+503", "key": "SV" }, { "iso3": "GNQ", "name": "Equatorial Guinea", "flag": "🇬🇶", "code": ["+240"], "search": "GQGNQEquatorial Guinea+240", "key": "GQ" }, { "iso3": "ERI", "name": "Eritrea", "flag": "🇪🇷", "code": ["+291"], "search": "ERERIEritrea+291", "key": "ER" }, { "iso3": "EST", "name": "Estonia", "flag": "🇪🇪", "code": ["+372"], "search": "EEESTEstonia+372", "key": "EE" }, { "iso3": "ETH", "name": "Ethiopia", "flag": "🇪🇹", "code": ["+251"], "search": "ETETHEthiopia+251", "key": "ET" }, { "iso3": "FLK", "name": "Falkland Islands", "flag": "🇫🇰", "code": ["+500"], "search": "FKFLKFalkland Islands+500", "key": "FK" }, { "iso3": "FRO", "name": "Faroe Islands", "flag": "🇫🇴", "code": ["+298"], "search": "FOFROFaroe Islands+298", "key": "FO" }, { "iso3": "FJI", "name": "Fiji", "flag": "🇫🇯", "code": ["+679"], "search": "FJFJIFiji+679", "key": "FJ" }, { "iso3": "FIN", "name": "Finland", "flag": "🇫🇮", "code": ["+358"], "search": "FIFINFinland+358", "key": "FI" }, { "iso3": "FRA", "name": "France", "flag": "🇫🇷", "code": ["+33"], "search": "FRFRAFrance+33", "key": "FR" }, { "iso3": "GUF", "name": "French Guiana", "flag": "🇬🇫", "code": ["+594"], "search": "GFGUFFrench Guiana+594", "key": "GF" }, { "iso3": "PYF", "name": "French Polynesia", "flag": "🇵🇫", "code": ["+689"], "search": "PFPYFFrench Polynesia+689", "key": "PF" }, { "iso3": "GAB", "name": "Gabon", "flag": "🇬🇦", "code": ["+241"], "search": "GAGABGabon+241", "key": "GA" }, { "iso3": "GMB", "name": "Gambia", "flag": "🇬🇲", "code": ["+220"], "search": "GMGMBGambia+220", "key": "GM" }, { "iso3": "GEO", "name": "Georgia", "flag": "🇬🇪", "code": ["+995"], "search": "GEGEOGeorgia+995", "key": "GE" }, { "iso3": "DEU", "name": "Germany", "flag": "🇩🇪", "code": ["+49"], "search": "DEDEUGermany+49", "key": "DE" }, { "iso3": "GHA", "name": "Ghana", "flag": "🇬🇭", "code": ["+233"], "search": "GHGHAGhana+233", "key": "GH" }, { "iso3": "GIB", "name": "Gibraltar", "flag": "🇬🇮", "code": ["+350"], "search": "GIGIBGibraltar+350", "key": "GI" }, { "iso3": "GRC", "name": "Greece", "flag": "🇬🇷", "code": ["+30"], "search": "GRGRCGreece+30", "key": "GR" }, { "iso3": "GRL", "name": "Greenland", "flag": "🇬🇱", "code": ["+299"], "search": "GLGRLGreenland+299", "key": "GL" }, { "iso3": "GRD", "name": "Grenada", "flag": "🇬🇩", "code": ["+1-473"], "search": "GDGRDGrenada+1-473", "key": "GD" }, { "iso3": "GLP", "name": "Guadeloupe", "flag": "🇬🇵", "code": ["+590"], "search": "GPGLPGuadeloupe+590", "key": "GP" }, { "iso3": "GUM", "name": "Guam", "flag": "🇬🇺", "code": ["+1-671"], "search": "GUGUMGuam+1-671", "key": "GU" }, { "iso3": "GTM", "name": "Guatemala", "flag": "🇬🇹", "code": ["+502"], "search": "GTGTMGuatemala+502", "key": "GT" }, { "iso3": "GGY", "name": "Guernsey", "flag": "🇬🇬", "code": ["+44-1481"], "search": "GGGGYGuernsey+44-1481", "key": "GG" }, { "iso3": "GIN", "name": "Guinea", "flag": "🇬🇳", "code": ["+224"], "search": "GNGINGuinea+224", "key": "GN" }, { "iso3": "GNB", "name": "Guinea-Bissau", "flag": "🇬🇼", "code": ["+245"], "search": "GWGNBGuinea-Bissau+245", "key": "GW" }, { "iso3": "GUY", "name": "Guyana", "flag": "🇬🇾", "code": ["+592"], "search": "GYGUYGuyana+592", "key": "GY" }, { "iso3": "HTI", "name": "Haiti", "flag": "🇭🇹", "code": ["+509"], "search": "HTHTIHaiti+509", "key": "HT" }, { "iso3": "HND", "name": "Honduras", "flag": "🇭🇳", "code": ["+504"], "search": "HNHNDHonduras+504", "key": "HN" }, { "iso3": "HKG", "name": "Hong Kong", "flag": "🇭🇰", "code": ["+852"], "search": "HKHKGHong Kong+852", "key": "HK" }, { "iso3": "HUN", "name": "Hungary", "flag": "🇭🇺", "code": ["+36"], "search": "HUHUNHungary+36", "key": "HU" }, { "iso3": "ISL", "name": "Iceland", "flag": "🇮🇸", "code": ["+354"], "search": "ISISLIceland+354", "key": "IS" }, { "iso3": "IND", "name": "India", "flag": "🇮🇳", "code": ["+91"], "search": "ININDIndia+91", "key": "IN" }, { "iso3": "IDN", "name": "Indonesia", "flag": "🇮🇩", "code": ["+62"], "search": "IDIDNIndonesia+62", "key": "ID" }, { "iso3": "IRN", "name": "Iran", "flag": "🇮🇷", "code": ["+98"], "search": "IRIRNIran+98", "key": "IR" }, { "iso3": "IRQ", "name": "Iraq", "flag": "🇮🇶", "code": ["+964"], "search": "IQIRQIraq+964", "key": "IQ" }, { "iso3": "IRL", "name": "Ireland", "flag": "🇮🇪", "code": ["+353"], "search": "IEIRLIreland+353", "key": "IE" }, { "iso3": "IMN", "name": "Isle of Man", "flag": "🇮🇲", "code": ["+44-1624"], "search": "IMIMNIsle of Man+44-1624", "key": "IM" }, { "iso3": "ISR", "name": "Israel", "flag": "🇮🇱", "code": ["+972"], "search": "ILISRIsrael+972", "key": "IL" }, { "iso3": "ITA", "name": "Italy", "flag": "🇮🇹", "code": ["+39"], "search": "ITITAItaly+39", "key": "IT" }, { "iso3": "CIV", "name": "Ivory Coast", "flag": "🇨🇮", "code": ["+225"], "search": "CICIVIvory Coast+225", "key": "CI" }, { "iso3": "JAM", "name": "Jamaica", "flag": "🇯🇲", "code": ["+1-876"], "search": "JMJAMJamaica+1-876", "key": "JM" }, { "iso3": "JPN", "name": "Japan", "flag": "🇯🇵", "code": ["+81"], "search": "JPJPNJapan+81", "key": "JP" }, { "iso3": "JEY", "name": "Jersey", "flag": "🇯🇪", "code": ["+44-1534"], "search": "JEJEYJersey+44-1534", "key": "JE" }, { "iso3": "JOR", "name": "Jordan", "flag": "🇯🇴", "code": ["+962"], "search": "JOJORJordan+962", "key": "JO" }, { "iso3": "KAZ", "name": "Kazakhstan", "flag": "🇰🇿", "code": ["+7"], "search": "KZKAZKazakhstan+7", "key": "KZ" }, { "iso3": "KEN", "name": "Kenya", "flag": "🇰🇪", "code": ["+254"], "search": "KEKENKenya+254", "key": "KE" }, { "iso3": "KIR", "name": "Kiribati", "flag": "🇰🇮", "code": ["+686"], "search": "KIKIRKiribati+686", "key": "KI" }, { "iso3": "KWT", "name": "Kuwait", "flag": "🇰🇼", "code": ["+965"], "search": "KWKWTKuwait+965", "key": "KW" }, { "iso3": "KGZ", "name": "Kyrgyzstan", "flag": "🇰🇬", "code": ["+996"], "search": "KGKGZKyrgyzstan+996", "key": "KG" }, { "iso3": "LAO", "name": "Laos", "flag": "🇱🇦", "code": ["+856"], "search": "LALAOLaos+856", "key": "LA" }, { "iso3": "LVA", "name": "Latvia", "flag": "🇱🇻", "code": ["+371"], "search": "LVLVALatvia+371", "key": "LV" }, { "iso3": "LBN", "name": "Lebanon", "flag": "🇱🇧", "code": ["+961"], "search": "LBLBNLebanon+961", "key": "LB" }, { "iso3": "LSO", "name": "Lesotho", "flag": "🇱🇸", "code": ["+266"], "search": "LSLSOLesotho+266", "key": "LS" }, { "iso3": "LBR", "name": "Liberia", "flag": "🇱🇷", "code": ["+231"], "search": "LRLBRLiberia+231", "key": "LR" }, { "iso3": "LBY", "name": "Libya", "flag": "🇱🇾", "code": ["+218"], "search": "LYLBYLibya+218", "key": "LY" }, { "iso3": "LIE", "name": "Liechtenstein", "flag": "🇱🇮", "code": ["+423"], "search": "LILIELiechtenstein+423", "key": "LI" }, { "iso3": "LTU", "name": "Lithuania", "flag": "🇱🇹", "code": ["+370"], "search": "LTLTULithuania+370", "key": "LT" }, { "iso3": "LUX", "name": "Luxembourg", "flag": "🇱🇺", "code": ["+352"], "search": "LULUXLuxembourg+352", "key": "LU" }, { "iso3": "MAC", "name": "Macao", "flag": "🇲🇴", "code": ["+853"], "search": "MOMACMacao+853", "key": "MO" }, { "iso3": "MKD", "name": "Macedonia", "flag": "🇲🇰", "code": ["+389"], "search": "MKMKDMacedonia+389", "key": "MK" }, { "iso3": "MDG", "name": "Madagascar", "flag": "🇲🇬", "code": ["+261"], "search": "MGMDGMadagascar+261", "key": "MG" }, { "iso3": "MWI", "name": "Malawi", "flag": "🇲🇼", "code": ["+265"], "search": "MWMWIMalawi+265", "key": "MW" }, { "iso3": "MYS", "name": "Malaysia", "flag": "🇲🇾", "code": ["+60"], "search": "MYMYSMalaysia+60", "key": "MY" }, { "iso3": "MDV", "name": "Maldives", "flag": "🇲🇻", "code": ["+960"], "search": "MVMDVMaldives+960", "key": "MV" }, { "iso3": "MLI", "name": "Mali", "flag": "🇲🇱", "code": ["+223"], "search": "MLMLIMali+223", "key": "ML" }, { "iso3": "MLT", "name": "Malta", "flag": "🇲🇹", "code": ["+356"], "search": "MTMLTMalta+356", "key": "MT" }, { "iso3": "MHL", "name": "Marshall Islands", "flag": "🇲🇭", "code": ["+692"], "search": "MHMHLMarshall Islands+692", "key": "MH" }, { "iso3": "MTQ", "name": "Martinique", "flag": "🇲🇶", "code": ["+596"], "search": "MQMTQMartinique+596", "key": "MQ" }, { "iso3": "MRT", "name": "Mauritania", "flag": "🇲🇷", "code": ["+222"], "search": "MRMRTMauritania+222", "key": "MR" }, { "iso3": "MUS", "name": "Mauritius", "flag": "🇲🇺", "code": ["+230"], "search": "MUMUSMauritius+230", "key": "MU" }, { "iso3": "MYT", "name": "Mayotte", "flag": "🇾🇹", "code": ["+262"], "search": "YTMYTMayotte+262", "key": "YT" }, { "iso3": "MEX", "name": "Mexico", "flag": "🇲🇽", "code": ["+52"], "search": "MXMEXMexico+52", "key": "MX" }, { "iso3": "FSM", "name": "Micronesia", "flag": "🇫🇲", "code": ["+691"], "search": "FMFSMMicronesia+691", "key": "FM" }, { "iso3": "MDA", "name": "Moldova", "flag": "🇲🇩", "code": ["+373"], "search": "MDMDAMoldova+373", "key": "MD" }, { "iso3": "MCO", "name": "Monaco", "flag": "🇲🇨", "code": ["+377"], "search": "MCMCOMonaco+377", "key": "MC" }, { "iso3": "MNG", "name": "Mongolia", "flag": "🇲🇳", "code": ["+976"], "search": "MNMNGMongolia+976", "key": "MN" }, { "iso3": "MNE", "name": "Montenegro", "flag": "🇲🇪", "code": ["+382"], "search": "MEMNEMontenegro+382", "key": "ME" }, { "iso3": "MSR", "name": "Montserrat", "flag": "🇲🇸", "code": ["+1-664"], "search": "MSMSRMontserrat+1-664", "key": "MS" }, { "iso3": "MAR", "name": "Morocco", "flag": "🇲🇦", "code": ["+212"], "search": "MAMARMorocco+212", "key": "MA" }, { "iso3": "MOZ", "name": "Mozambique", "flag": "🇲🇿", "code": ["+258"], "search": "MZMOZMozambique+258", "key": "MZ" }, { "iso3": "MMR", "name": "Myanmar", "flag": "🇲🇲", "code": ["+95"], "search": "MMMMRMyanmar+95", "key": "MM" }, { "iso3": "NAM", "name": "Namibia", "flag": "🇳🇦", "code": ["+264"], "search": "NANAMNamibia+264", "key": "NA" }, { "iso3": "NRU", "name": "Nauru", "flag": "🇳🇷", "code": ["+674"], "search": "NRNRUNauru+674", "key": "NR" }, { "iso3": "NPL", "name": "Nepal", "flag": "🇳🇵", "code": ["+977"], "search": "NPNPLNepal+977", "key": "NP" }, { "iso3": "NLD", "name": "Netherlands", "flag": "🇳🇱", "code": ["+31"], "search": "NLNLDNetherlands+31", "key": "NL" }, { "iso3": "NCL", "name": "New Caledonia", "flag": "🇳🇨", "code": ["+687"], "search": "NCNCLNew Caledonia+687", "key": "NC" }, { "iso3": "NZL", "name": "New Zealand", "flag": "🇳🇿", "code": ["+64"], "search": "NZNZLNew Zealand+64", "key": "NZ" }, { "iso3": "NIC", "name": "Nicaragua", "flag": "🇳🇮", "code": ["+505"], "search": "NINICNicaragua+505", "key": "NI" }, { "iso3": "NER", "name": "Niger", "flag": "🇳🇪", "code": ["+227"], "search": "NENERNiger+227", "key": "NE" }, { "iso3": "NGA", "name": "Nigeria", "flag": "🇳🇬", "code": ["+234"], "search": "NGNGANigeria+234", "key": "NG" }, { "iso3": "NIU", "name": "Niue", "flag": "🇳🇺", "code": ["+683"], "search": "NUNIUNiue+683", "key": "NU" }, { "iso3": "NFK", "name": "Norfolk Island", "flag": "🇳🇫", "code": ["+672"], "search": "NFNFKNorfolk Island+672", "key": "NF" }, { "iso3": "PRK", "name": "North Korea", "flag": "🇰🇵", "code": ["+850"], "search": "KPPRKNorth Korea+850", "key": "KP" }, { "iso3": "MNP", "name": "Northern Mariana Islands", "flag": "🇲🇵", "code": ["+1-670"], "search": "MPMNPNorthern Mariana Islands+1-670", "key": "MP" }, { "iso3": "NOR", "name": "Norway", "flag": "🇳🇴", "code": ["+47"], "search": "NONORNorway+47", "key": "NO" }, { "iso3": "OMN", "name": "Oman", "flag": "🇴🇲", "code": ["+968"], "search": "OMOMNOman+968", "key": "OM" }, { "iso3": "PAK", "name": "Pakistan", "flag": "🇵🇰", "code": ["+92"], "search": "PKPAKPakistan+92", "key": "PK" }, { "iso3": "PLW", "name": "Palau", "flag": "🇵🇼", "code": ["+680"], "search": "PWPLWPalau+680", "key": "PW" }, { "iso3": "PSE", "name": "Palestinian Territory", "flag": "🇵🇸", "code": ["+970"], "search": "PSPSEPalestinian Territory+970", "key": "PS" }, { "iso3": "PAN", "name": "Panama", "flag": "🇵🇦", "code": ["+507"], "search": "PAPANPanama+507", "key": "PA" }, { "iso3": "PNG", "name": "Papua New Guinea", "flag": "🇵🇬", "code": ["+675"], "search": "PGPNGPapua New Guinea+675", "key": "PG" }, { "iso3": "PRY", "name": "Paraguay", "flag": "🇵🇾", "code": ["+595"], "search": "PYPRYParaguay+595", "key": "PY" }, { "iso3": "PER", "name": "Peru", "flag": "🇵🇪", "code": ["+51"], "search": "PEPERPeru+51", "key": "PE" }, { "iso3": "PHL", "name": "Philippines", "flag": "🇵🇭", "code": ["+63"], "search": "PHPHLPhilippines+63", "key": "PH" }, { "iso3": "PCN", "name": "Pitcairn", "flag": "🇵🇳", "code": ["+870"], "search": "PNPCNPitcairn+870", "key": "PN" }, { "iso3": "POL", "name": "Poland", "flag": "🇵🇱", "code": ["+48"], "search": "PLPOLPoland+48", "key": "PL" }, { "iso3": "PRT", "name": "Portugal", "flag": "🇵🇹", "code": ["+351"], "search": "PTPRTPortugal+351", "key": "PT" }, { "iso3": "PRI", "name": "Puerto Rico", "flag": "🇵🇷", "code": ["+1-787", "+ 1-939"], "search": "PRPRIPuerto Rico+1-787,+ 1-939", "key": "PR" }, { "iso3": "QAT", "name": "Qatar", "flag": "🇶🇦", "code": ["+974"], "search": "QAQATQatar+974", "key": "QA" }, { "iso3": "COG", "name": "Republic of the Congo", "flag": "🇨🇬", "code": ["+242"], "search": "CGCOGRepublic of the Congo+242", "key": "CG" }, { "iso3": "REU", "name": "Reunion", "flag": "🇷🇪", "code": ["+262"], "search": "REREUReunion+262", "key": "RE" }, { "iso3": "ROU", "name": "Romania", "flag": "🇷🇴", "code": ["+40"], "search": "ROROURomania+40", "key": "RO" }, { "iso3": "RUS", "name": "Russia", "flag": "🇷🇺", "code": ["+7"], "search": "RURUSRussia+7", "key": "RU" }, { "iso3": "RWA", "name": "Rwanda", "flag": "🇷🇼", "code": ["+250"], "search": "RWRWARwanda+250", "key": "RW" }, { "iso3": "BLM", "name": "Saint Barthelemy", "flag": "🇧🇱", "code": ["+590"], "search": "BLBLMSaint Barthelemy+590", "key": "BL" }, { "iso3": "SHN", "name": "Saint Helena", "flag": "🇸🇭", "code": ["+290"], "search": "SHSHNSaint Helena+290", "key": "SH" }, { "iso3": "KNA", "name": "Saint Kitts and Nevis", "flag": "🇰🇳", "code": ["+1-869"], "search": "KNKNASaint Kitts and Nevis+1-869", "key": "KN" }, { "iso3": "LCA", "name": "Saint Lucia", "flag": "🇱🇨", "code": ["+1-758"], "search": "LCLCASaint Lucia+1-758", "key": "LC" }, { "iso3": "MAF", "name": "Saint Martin", "flag": "🇲🇫", "code": ["+590"], "search": "MFMAFSaint Martin+590", "key": "MF" }, { "iso3": "SPM", "name": "Saint Pierre and Miquelon", "flag": "🇵🇲", "code": ["+508"], "search": "PMSPMSaint Pierre and Miquelon+508", "key": "PM" }, { "iso3": "VCT", "name": "Saint Vincent and the Grenadines", "flag": "🇻🇨", "code": ["+1-784"], "search": "VCVCTSaint Vincent and the Grenadines+1-784", "key": "VC" }, { "iso3": "WSM", "name": "Samoa", "flag": "🇼🇸", "code": ["+685"], "search": "WSWSMSamoa+685", "key": "WS" }, { "iso3": "SMR", "name": "San Marino", "flag": "🇸🇲", "code": ["+378"], "search": "SMSMRSan Marino+378", "key": "SM" }, { "iso3": "STP", "name": "Sao Tome and Principe", "flag": "🇸🇹", "code": ["+239"], "search": "STSTPSao Tome and Principe+239", "key": "ST" }, { "iso3": "SAU", "name": "Saudi Arabia", "flag": "🇸🇦", "code": ["+966"], "search": "SASAUSaudi Arabia+966", "key": "SA" }, { "iso3": "SEN", "name": "Senegal", "flag": "🇸🇳", "code": ["+221"], "search": "SNSENSenegal+221", "key": "SN" }, { "iso3": "SRB", "name": "Serbia", "flag": "🇷🇸", "code": ["+381"], "search": "RSSRBSerbia+381", "key": "RS" }, { "iso3": "SYC", "name": "Seychelles", "flag": "🇸🇨", "code": ["+248"], "search": "SCSYCSeychelles+248", "key": "SC" }, { "iso3": "SLE", "name": "Sierra Leone", "flag": "🇸🇱", "code": ["+232"], "search": "SLSLESierra Leone+232", "key": "SL" }, { "iso3": "SGP", "name": "Singapore", "flag": "🇸🇬", "code": ["+65"], "search": "SGSGPSingapore+65", "key": "SG" }, { "iso3": "SXM", "name": "Sint Maarten", "flag": "🇸🇽", "code": ["+599"], "search": "SXSXMSint Maarten+599", "key": "SX" }, { "iso3": "SVK", "name": "Slovakia", "flag": "🇸🇰", "code": ["+421"], "search": "SKSVKSlovakia+421", "key": "SK" }, { "iso3": "SVN", "name": "Slovenia", "flag": "🇸🇮", "code": ["+386"], "search": "SISVNSlovenia+386", "key": "SI" }, { "iso3": "SLB", "name": "Solomon Islands", "flag": "🇸🇧", "code": ["+677"], "search": "SBSLBSolomon Islands+677", "key": "SB" }, { "iso3": "SOM", "name": "Somalia", "flag": "🇸🇴", "code": ["+252"], "search": "SOSOMSomalia+252", "key": "SO" }, { "iso3": "ZAF", "name": "South Africa", "flag": "🇿🇦", "code": ["+27"], "search": "ZAZAFSouth Africa+27", "key": "ZA" }, { "iso3": "KOR", "name": "South Korea", "flag": "🇰🇷", "code": ["+82"], "search": "KRKORSouth Korea+82", "key": "KR" }, { "iso3": "SSD", "name": "South Sudan", "flag": "🇸🇸", "code": ["+211"], "search": "SSSSDSouth Sudan+211", "key": "SS" }, { "iso3": "ESP", "name": "Spain", "flag": "🇪🇸", "code": ["+34"], "search": "ESESPSpain+34", "key": "ES" }, { "iso3": "LKA", "name": "Sri Lanka", "flag": "🇱🇰", "code": ["+94"], "search": "LKLKASri Lanka+94", "key": "LK" }, { "iso3": "SDN", "name": "Sudan", "flag": "🇸🇩", "code": ["+249"], "search": "SDSDNSudan+249", "key": "SD" }, { "iso3": "SUR", "name": "Suriname", "flag": "🇸🇷", "code": ["+597"], "search": "SRSURSuriname+597", "key": "SR" }, { "iso3": "SJM", "name": "Svalbard and Jan Mayen", "flag": "🇸🇯", "code": ["+47"], "search": "SJSJMSvalbard and Jan Mayen+47", "key": "SJ" }, { "iso3": "SWZ", "name": "Swaziland", "flag": "🇸🇿", "code": ["+268"], "search": "SZSWZSwaziland+268", "key": "SZ" }, { "iso3": "SWE", "name": "Sweden", "flag": "🇸🇪", "code": ["+46"], "search": "SESWESweden+46", "key": "SE" }, { "iso3": "CHE", "name": "Switzerland", "flag": "🇨🇭", "code": ["+41"], "search": "CHCHESwitzerland+41", "key": "CH" }, { "iso3": "SYR", "name": "Syria", "flag": "🇸🇾", "code": ["+963"], "search": "SYSYRSyria+963", "key": "SY" }, { "iso3": "TWN", "name": "Taiwan", "flag": "🇹🇼", "code": ["+886"], "search": "TWTWNTaiwan+886", "key": "TW" }, { "iso3": "TJK", "name": "Tajikistan", "flag": "🇹🇯", "code": ["+992"], "search": "TJTJKTajikistan+992", "key": "TJ" }, { "iso3": "TZA", "name": "Tanzania", "flag": "🇹🇿", "code": ["+255"], "search": "TZTZATanzania+255", "key": "TZ" }, { "iso3": "THA", "name": "Thailand", "flag": "🇹🇭", "code": ["+66"], "search": "THTHAThailand+66", "key": "TH" }, { "iso3": "TGO", "name": "Togo", "flag": "🇹🇬", "code": ["+228"], "search": "TGTGOTogo+228", "key": "TG" }, { "iso3": "TKL", "name": "Tokelau", "flag": "🇹🇰", "code": ["+690"], "search": "TKTKLTokelau+690", "key": "TK" }, { "iso3": "TON", "name": "Tonga", "flag": "🇹🇴", "code": ["+676"], "search": "TOTONTonga+676", "key": "TO" }, { "iso3": "TTO", "name": "Trinidad and Tobago", "flag": "🇹🇹", "code": ["+1-868"], "search": "TTTTOTrinidad and Tobago+1-868", "key": "TT" }, { "iso3": "TUN", "name": "Tunisia", "flag": "🇹🇳", "code": ["+216"], "search": "TNTUNTunisia+216", "key": "TN" }, { "iso3": "TUR", "name": "Turkey", "flag": "🇹🇷", "code": ["+90"], "search": "TRTURTurkey+90", "key": "TR" }, { "iso3": "TKM", "name": "Turkmenistan", "flag": "🇹🇲", "code": ["+993"], "search": "TMTKMTurkmenistan+993", "key": "TM" }, { "iso3": "TCA", "name": "Turks and Caicos Islands", "flag": "🇹🇨", "code": ["+1-649"], "search": "TCTCATurks and Caicos Islands+1-649", "key": "TC" }, { "iso3": "TUV", "name": "Tuvalu", "flag": "🇹🇻", "code": ["+688"], "search": "TVTUVTuvalu+688", "key": "TV" }, { "iso3": "VIR", "name": "U.S. Virgin Islands", "flag": "🇻🇮", "code": ["+1-340"], "search": "VIVIRU.S. Virgin Islands+1-340", "key": "VI" }, { "iso3": "UGA", "name": "Uganda", "flag": "🇺🇬", "code": ["+256"], "search": "UGUGAUganda+256", "key": "UG" }, { "iso3": "UKR", "name": "Ukraine", "flag": "🇺🇦", "code": ["+380"], "search": "UAUKRUkraine+380", "key": "UA" }, { "iso3": "ARE", "name": "United Arab Emirates", "flag": "🇦🇪", "code": ["+971"], "search": "AEAREUnited Arab Emirates+971", "key": "AE" }, { "iso3": "GBR", "name": "United Kingdom", "flag": "🇬🇧", "code": ["+44"], "search": "GBGBRUnited Kingdom+44", "key": "GB" }, { "iso3": "USA", "name": "United States", "flag": "🇺🇸", "code": ["+1"], "search": "USUSAUnited States+1", "key": "US" }, { "iso3": "UMI", "name": "United States Minor Outlying Islands", "flag": "🇺🇲", "code": ["+1"], "search": "UMUMIUnited States Minor Outlying Islands+1", "key": "UM" }, { "iso3": "URY", "name": "Uruguay", "flag": "🇺🇾", "code": ["+598"], "search": "UYURYUruguay+598", "key": "UY" }, { "iso3": "UZB", "name": "Uzbekistan", "flag": "🇺🇿", "code": ["+998"], "search": "UZUZBUzbekistan+998", "key": "UZ" }, { "iso3": "VUT", "name": "Vanuatu", "flag": "🇻🇺", "code": ["+678"], "search": "VUVUTVanuatu+678", "key": "VU" }, { "iso3": "VAT", "name": "Vatican", "flag": "🇻🇦", "code": ["+379"], "search": "VAVATVatican+379", "key": "VA" }, { "iso3": "VEN", "name": "Venezuela", "flag": "🇻🇪", "code": ["+58"], "search": "VEVENVenezuela+58", "key": "VE" }, { "iso3": "VNM", "name": "Vietnam", "flag": "🇻🇳", "code": ["+84"], "search": "VNVNMVietnam+84", "key": "VN" }, { "iso3": "WLF", "name": "Wallis and Futuna", "flag": "🇼🇫", "code": ["+681"], "search": "WFWLFWallis and Futuna+681", "key": "WF" }, { "iso3": "ESH", "name": "Western Sahara", "flag": "🇪🇭", "code": ["+212"], "search": "EHESHWestern Sahara+212", "key": "EH" }, { "iso3": "YEM", "name": "Yemen", "flag": "🇾🇪", "code": ["+967"], "search": "YEYEMYemen+967", "key": "YE" }, { "iso3": "ZMB", "name": "Zambia", "flag": "🇿🇲", "code": ["+260"], "search": "ZMZMBZambia+260", "key": "ZM" }, { "iso3": "ZWE", "name": "Zimbabwe", "flag": "🇿🇼", "code": ["+263"], "search": "ZWZWEZimbabwe+263", "key": "ZW" }];


const selectElement = document.querySelector('.select');
const iconElement = selectElement.querySelector('tg-icon');
const inputElement = selectElement.querySelector('input');
const labelElement = selectElement.querySelector('label');
const optionsListElement = selectElement.querySelector('div.options-list');

let originalValue = null; // for hovering after selection

//populating countries
for (let index = 0; index < countryList.length; index++) {
    const country = countryList[index];

    const optionElement = document.createElement('div');
    optionElement.classList.add('country', 'option');
    optionElement.dataset.search = country.search;
    optionElement.dataset.index = index;
    optionElement.innerHTML = `
        <span class="flag">${country.flag}</span>
        <span class="name">${country.name}</span>
        <span class="code">${country.code[0]}</span>
    `;

    optionsListElement.appendChild(optionElement);

    optionElement.addEventListener('mousemove', function () {
        const selectedElement = selectElement.querySelector('.option.selected');
        if (selectedElement && selectedElement !== this) {
            selectedElement.classList.remove('selected');
        }

        this.classList.add('selected');

        if (!inputElement.value || inputElement.value.length === 0) {
            labelElement.innerText = country.name;
        } else {
            labelElement.innerText = 'Country';
            originalValue = originalValue || inputElement.value;
            inputElement.value = country.name;
        }
    });

    optionElement.addEventListener('mouseleave', function () {
        this.classList.remove('selected');

        if (!inputElement.value || inputElement.value.length === 0) {
            labelElement.innerText = 'Country';
        } else {
            inputElement.value = originalValue || inputElement.value;
            originalValue = null;
        }
    });

    optionElement.addEventListener('mousedown', (event) => {
        event.stopPropagation();
    });

    optionElement.addEventListener('click', function (event) {
        event.stopPropagation();
        this.classList.add('selected');
        chooseValue(country.name, index);
    });
}

//menu click events
inputElement.addEventListener('mousedown', (event) => {
    event.stopPropagation();
    toggleMenu();
});

document.body.addEventListener('mousedown', () => {
    closeMenu();
});

function chooseValue(value, index, notify = true) {
    labelElement.innerText = 'Country';

    inputElement.classList.remove('wrong');
    inputElement.value = value;

    originalValue = null;

    const hiddenOptions = selectElement.querySelectorAll('.option.hidden');
    hiddenOptions.forEach(option => void option.classList.remove('hidden'));

    inputElement.dataset.index = index;
    if (notify) {
        inputElement.dispatchEvent(new Event('selection-done'));
    }

    closeMenu();
}

function isMenuOpen() {
    return iconElement.getAttribute('name') === 'up';
}

function toggleMenu() {
    iconElement.setAttribute('name', isMenuOpen() ? 'down' : 'up');
    optionsListElement.classList.toggle('hidden');
}

function closeMenu() {
    if (isMenuOpen()) {
        toggleMenu();
    }
}

function openMenu() {
    if (!isMenuOpen()) {
        toggleMenu();
    }
}

//autocomplete
inputElement.addEventListener('input', handleInputInput);
inputElement.addEventListener('change', handleInputChange);

function handleInputInput(event) {
    const selectedOption = selectElement.querySelector('.country.option.selected');

    openMenu();

    originalValue = null;
    const { value } = event.target;
    const searchText = value.replace('+', '\\+');

    const allCountries = selectElement.querySelectorAll('.country.option');
    for (const option of allCountries) {
        if (option.dataset.search.match(new RegExp(searchText, 'i'))) {
            option.classList.remove('hidden');
        } else {
            if (option === selectedOption) {
                selectedOption.classList.remove('selected');
            }
            option.classList.add('hidden');
        }
    }
}

function handleInputChange(event) {
    const selectedCountryIndex = countryList.findIndex(country => country.name.toLowerCase() === event.target.value.toLowerCase())

    if (selectedCountryIndex !== -1) {
        chooseValue(countryList[selectedCountryIndex].name, selectedCountryIndex);
    } else {
        inputElement.classList.add('wrong');
    }
}

// keyboard bindings
document.addEventListener('keydown', (event) => {
    if (isMenuOpen()) {
        const availableOptions = selectElement.querySelectorAll('.country.option:not(.hidden)');

        const selectedIndex = Array.from(availableOptions).findIndex((element) => element.classList.contains('selected'));
        let selected;

        switch (event.key) {
            case 'ArrowUp':
                if (selectedIndex === -1 && availableOptions.length > 0) {
                    selected = availableOptions[availableOptions.length - 1];
                } else {
                    availableOptions[selectedIndex].classList.remove('selected');

                    if (selectedIndex !== 0) {
                        selected = availableOptions[selectedIndex - 1];
                    } else {
                        selected = availableOptions[availableOptions.length - 1];
                    }
                }
                break;
            case 'ArrowDown':
                if (selectedIndex === -1 && availableOptions.length > 0) {
                    selected = availableOptions[0];
                } else {
                    availableOptions[selectedIndex].classList.remove('selected');
                    if (selectedIndex < availableOptions.length - 1) {
                        selected = availableOptions[selectedIndex + 1];
                    } else {
                        selected = availableOptions[0];
                    }
                }
                break;
            case 'Enter':
                if (selectedIndex !== -1) {
                    event.stopPropagation();

                    chooseValue(
                        availableOptions[selectedIndex].querySelector('.name').innerText,
                        availableOptions[selectedIndex].dataset.index
                    );
                }
                break;
            default:
                break;
        }

        if (selected) {
            selected.classList.add('selected');
            selected.scrollIntoView(false);
        }
    }
})


selectElement.addEventListener('keydown', (event) => {
    switch (event.key) {
        case 'ArrowUp':
        case 'ArrowDown':
            openMenu();
        case 'Enter':
            event.preventDefault();
        default:
            break;
    }
})
