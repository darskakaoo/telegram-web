(function() {
    const form = document.querySelector('form');
    const countryInput = document.getElementById('country');
    const phoneInput = document.getElementById('phone');
    const submitButton = form.querySelector('button[type="submit"]');
    const tgLoading = submitButton.querySelector('tg-loading');

    // submitButton.addEventListener('click', () => {

    // });

    countryInput.addEventListener('selection-done', () => {
        phoneInput.value = countryList[countryInput.dataset.index].code[0] + ' ';
        phoneInput.focus();
        handleChange();
    });

    phoneInput.addEventListener('input', () => {
        const probableCountryCode = phoneInput.value;

        theFor: for (let index = 0; index < countryList.length; index++) {
            const country = countryList[index];

            for (const code of country.code) {
                if (probableCountryCode.startsWith(code)) {
                    if (index !== countryInput.dataset.index) {
                        chooseValue(country.name, index, false);
                        break theFor;
                    }
                }
            }
        }
    });

    function handleChange() {
        if (
            !phoneInput.value ||
            !countryInput.value ||
            countryInput.classList.contains('wrong') ||
            countryList[countryInput.dataset.index].code.indexOf(phoneInput.value.trim()) !== -1
        ) {
            submitButton.classList.add('invisible');
        } else {
            submitButton.classList.remove('invisible');
        }
    }

    countryInput.addEventListener('input', handleChange);
    phoneInput.addEventListener('input', handleChange);

    bindForm(
        form,
        ({ phone_code_hash, phone_number }) => {
            navigate('code', { phone_code_hash, phone_number });
        },
        console.error,
        values => {
            tgLoading.classList.remove('invisible');
            submitButton.disabled = true;
            return {
                ...values,
                phone: values.phone.replace(/\s*/, '')
            };
        }
    );
})();
