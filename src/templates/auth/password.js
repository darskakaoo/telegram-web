(function () {
    const passwordVisibility = document.querySelector('.input tg-icon');
    const passwordInput = document.querySelector('input[name="password"]');
    const monkeyPlayer = document.querySelector('tgs-player');
    let target = null;

    let monkeyState = 'idle';

    function setMonkeyState({ state, loop = false, autoPlay = false, direction, until, from }) {
        monkeyState = state;

        monkeyPlayer.autoplay = autoPlay;

        const capitalizedState = state.slice(0, 1).toUpperCase() + state.slice(1);
        monkeyPlayer.load(`/animations/TwoFactorSetupMonkey${capitalizedState}.tgs`).then(() => {
            // monkeyPlayer.stop();
            if (loop) {
                monkeyPlayer.loop = true;
            } else {
                monkeyPlayer.loop = false;
            }

            if (direction) {
                monkeyPlayer.setDirection(direction);
            }

            if (until) {
                function frameEventListener({ detail: { frame } }) {
                    if (Math.abs(frame - until) < 0.1) {
                        monkeyPlayer.pause();
                        monkeyPlayer.removeEventListener('frame', frameEventListener);
                    }
                }

                monkeyPlayer.addEventListener('frame', frameEventListener);
            }

            if (from) {
                monkeyPlayer.getLottie().goToAndPlay(from, true);
            } else {
                monkeyPlayer.play();
            }

        });


    }

    passwordInput.addEventListener('focus', () => {
        if (target === 'blur-visibility') {
            if (passwordIsVisible()) {
                setMonkeyState({ state: 'peek', until: 16 });
            } else {
                setMonkeyState({ state: 'peek', from: 17 });
            }

            target = null;
            return;
        } else if (target === 'visibility') {
            target = null;
        }

        if (passwordIsVisible()) {
            setMonkeyState({ state: 'closeAndPeek' })
        } else {
            setMonkeyState({ state: 'close', until: 45 });
        }
    });

    passwordInput.addEventListener('blur', () => {
        if (target === 'visibility') {
            target = 'blur-visibility';
            return;
        }

        if (passwordIsVisible()) {
            setMonkeyState({ state: 'closeAndPeekToIdle' })
        } else {
            setMonkeyState({ state: 'close', from: 45 });
        }
    });

    function passwordIsVisible() {
        const name = passwordVisibility.getAttribute('name');
        if (name === 'eye2') {
            return false;
        } else {
            return true;
        }
    }

    passwordVisibility.addEventListener('mousedown', () => {
        target = 'visibility';
    });

    passwordVisibility.addEventListener('click', () => {
        if (!passwordIsVisible()) {
            passwordVisibility.setAttribute('name', 'eye1');
            passwordInput.type = 'text';
        } else {
            passwordVisibility.setAttribute('name', 'eye2');
            passwordInput.type = 'password';
        }

        passwordInput.focus();
    });
})();
