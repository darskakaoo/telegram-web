(
    function () {

        callAPI('account.updateStatus', {
            offline: false
        });

        const chatElement = document.getElementById('chat');

        const { showLoading } = context;
        if (showLoading) {
            callAPI('messages.getDialogs', {}, setState);
        }

        const messagePreviews = document.querySelectorAll('.chat-preview');

        messagePreviews.forEach(preview => {
            const peerId = preview.dataset.peerId;

            preview.addEventListener('mousedown', (event) => {
                const { target, layerX, layerY } = event;
                const ripple = document.createElement('div');
                ripple.classList.add('ripple');
                target.appendChild(ripple);
                ripple.classList.add('active');
                ripple.style = `top: ${layerY}px;left: ${layerX}px;`;

                setTimeout(() => {
                    ripple.remove();
                }, 1000);
            });

            preview.addEventListener('click', function () {
                if (!this.classList.contains('active')) {
                    const lastActive = document.querySelector('.chat-preview.active');
                    if (lastActive) {
                        lastActive.classList.remove('active');
                    }

                    preview.classList.add('active');
                    render('chat/message/messages', {
                        activePeerId: peerId
                    },
                        chatElement
                    ).then(() => {
                        history.pushState({}, '', `/messages/${peerId}/`);
                    });
                }
            });
        });
    }
)();