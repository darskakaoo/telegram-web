(
    function () {
        let peer;
        let currentMessageGroup;
        // load messages, remove loading and get history
        const chatContainerElement = document.getElementById('chat-container');
        function addMessage(messageElement, parent = chatContainerElement) {
            parent.appendChild(messageElement);

            if (parent !== chatContainerElement) {
                parent.scrollIntoView(false);
            } else {
                messageElement.scrollIntoView(false);
            }
        }

        const { activePeerId } = context;
        const peerId = parseInt(activePeerId);

        const chatElement = document.getElementById('chat');
        const loadingElement = document.querySelector('tg-loading');
        const chatPreviewUnreadCountElement = document.querySelector('.chat-preview .unread-count');

        const peerInfoElement = document.getElementById('peer-info');


        // load top bar elements
        const chatPreviewElement = document.querySelector(`.chat-preview[data-peer-id="${peerId}"]`);

        // name
        peerInfoElement.querySelector('.title').innerHTML = chatPreviewElement.querySelector('.title').innerHTML;

        // copy image and insert into topbar
        const copy = chatPreviewElement.querySelector('tg-profile-image').cloneNode(true);
        peerInfoElement.querySelector('.profile-image-container').appendChild(copy);

        peerInfoElement.classList.remove('invisible');
        chatElement.classList.remove('bottom-aligned');

        findPeer(peerId).then(resultPeer => {
            peer = createPeer(resultPeer.constructor.name.toLowerCase(), resultPeer.id, resultPeer.accessHash);

            // set user status or chat member count
            let status = '';
            let color = 'grey';
            if (peer._.indexOf('User') !== -1) {
                if (resultPeer.status) {
                    if (resultPeer.status.status === 'online') {
                        status = 'online';
                        color = 'accent';
                    }
                    else if (resultPeer.status.status === 'offline') {
                        const date = new Date(resultPeer.status.lastSeen).toDateString();
                        status = `last seen ${date}`;
                    } else {
                        status = resultPeer.status.status;
                    }
                } else {
                    status = 'bot';
                }
            } else {
                status = `${resultPeer.memberCount} Members`;
            }

            const statusElement = peerInfoElement.querySelector('.status');
            statusElement.innerHTML = status;
            statusElement.classList.add(color);

            callAPI('messages.getHistory', { peer }, ({ messages }) => {
                if (loadingElement) {
                    loadingElement.remove();
                }

                chatElement.classList.add('bottom-aligned');

                if (messages.length > 0) {

                    messages.push({ fromId: -1 });
                    const messageGroups = [];
                    currentMessageGroup = [];
                    let currentPeerId = messages[0].fromId;

                    messages.forEach(message => {
                        if (message.fromId !== currentPeerId) {
                            messageGroups.push(currentMessageGroup);
                            currentMessageGroup = [];
                            currentPeerId = message.fromId;
                        }

                        currentMessageGroup.push(message);
                    });

                    currentMessageGroup = messageGroups[messageGroups.length - 1];

                    messageGroups.forEach(messageGroup => {
                        const helperDiv = document.createElement('div');

                        const { self, id } = messageGroup[0];

                        render('chat/message/messageGroup', {
                            messageGroup,
                            self,
                            id,
                        }, helperDiv).then(() => {
                            const result = helperDiv.children[0];
                            addMessage(result);
                        });
                    });

                    if (chatPreviewUnreadCountElement) {
                        callAPI('messages.readHistory', { peer }, () => {
                            chatPreviewUnreadCountElement.remove();
                        });
                    }
                }
            })
        });

        // form bindings
        let randomId;
        const form = document.querySelector('form');
        const sendMessageButton = document.getElementById('send-message');
        const messageInputElement = document.getElementById('message');

        messageInputElement.addEventListener('input', () => {
            if (messageInputElement.value.trim().length > 0) {
                if (!randomId) {
                    randomId = generateRandomId();
                }

                sendMessageButton.setAttribute('type', 'submit');
                sendMessageButton.children[0].setAttribute('name', 'send');
            } else {
                sendMessageButton.setAttribute('type', 'button');
                sendMessageButton.children[0].setAttribute('name', 'microphone');
            }
        });

        bindForm(form, ({ message }) => {
            const helperDiv = document.createElement('div');
            const messageObject = {
                message: message.message,
                date: message.date
            };

            if (currentMessageGroup[0].self === 'self') {
                const lastGroup = document.querySelector('.bubble-group:last-child');

                render('chat/message/singlemessage', messageObject, helperDiv).then(() => {
                    const result = helperDiv.children[0];
                    addMessage(result, lastGroup);
                });

            } else {

                render('chat/message/messageGroup', {
                    messageGroup: [
                        messageObject
                    ],
                    self: 'self',
                    id: message.id,
                }, helperDiv).then(() => {
                    const result = helperDiv.children[0];
                    addMessage(result);
                });
            }

        }, console.error, ({ message }) => {
            form.reset();
            sendMessageButton.children[0].setAttribute('name', 'microphone');
            sendMessageButton.setAttribute('type', 'button');

            return {
                message: message.trim(),
                randomId,
                peer
            };
        });
    }
)();