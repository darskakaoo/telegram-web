import readTemplate from './core/read-template';
import removeComments from './core/template-tags/Comment';
import parseControlFlow from './core/template-tags/control-flow';
import parseExtend from './core/template-tags/extend';
import parseIncludes from './core/template-tags/include';
import parseVariables from './core/variable';

export async function render(template, context = {}, selector = document.body, templateIsString = false, removeBlocks = true) {

    try {
        // get template
        let templateString = await readTemplate(template, templateIsString);

        // remove comments
        templateString = removeComments(templateString);

        // resolve variables
        templateString = parseVariables(templateString, context);

        // resolve extends
        templateString = await parseExtend(templateString, context, templateIsString, removeBlocks);

        // resolve control flows
        templateString = await parseControlFlow(templateString, context);

        // resolve includes
        templateString = await parseIncludes(templateString, context);

        // expose context to window so that scripts can use it
        window.context = context;

        if (selector) {
            // render into an HTMLElement
            let parentElement;
            if (typeof(selector) === 'string') {
                parentElement = document.querySelector(selector);
            } else {
                parentElement = selector;
            }

            if (parentElement) {
                parentElement.innerHTML = templateString;

                // remove script string and create HTMLScriptElements so that they can run
                const allScripts = parentElement.querySelectorAll('script');
                for (const script of allScripts) {
                    script.parentNode.removeChild(script);
                    const newScript = document.createElement('script');

                    if (script.innerHTML.trim()) {
                        const scriptText = script.innerHTML;
                        newScript.innerHTML = scriptText;
                    } else if (script.getAttribute('src')) {
                        newScript.setAttribute('src', script.getAttribute('src'));
                    }

                    parentElement.appendChild(newScript);
                }

                // add template router too all links
                addRouterToLinks(parentElement);
            }
        } else {
            // return the rendered code
            return templateString;
        }

    } catch (error) {
        console.error(`Error trying to render template ${template}`);
        throw error;
    }
}

export function addRouterToLinks(parentElement = document.body) {
    const allLinks = parentElement.querySelectorAll('a');

    allLinks.forEach(link => {
        link.addEventListener('click', (event) => {
            const href = link.href;

            if (href.startsWith(window.location.origin)) {
                event.preventDefault();

                history.pushState({}, '', href);
                handleRoute(href);
            }
        });
    });
}

window.render = render;
