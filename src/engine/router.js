import RandExp from 'randexp';

let routes;

export function setRoutes(selectedRoutes) {
    routes = selectedRoutes;

    window.addEventListener('popstate', function () {
        if (window.location.href.startsWith(window.location.origin)) {
            handleRoute(window.location.href);
        }
    });

    window.navigate = navigate;
    window.setState = setState;
    handleRoute(window.location.href);
}

export async function handleRoute(path) {
    // TODO: should use not found page
    if (path.startsWith(window.origin)) {
        path = path.substring(window.origin.length + 1);
    }

    if (path.length > 0 && !path.endsWith('/')) {
        path = path + '/';
    }

    if (path.length > 0 && !path.startsWith('/')) {
        path = '/' + path;
    }

    for (const route of routes) {
        const match = path.match(route.path);
        if (match) {
            await route.handler(
                ...Array.from(match).slice(1),
                {
                    state: history.state || {},
                    ...(match.groups || {})
                },
            );

            break;
        }
    }
}

export function setState(state) {
    const newState = {
        ...history.state,
        ...state
    };

    const path = window.location.href;

    history.replaceState(newState, '', path);
    handleRoute(path);
}

export function navigate(path = window.location.href, state = {}) {
    path = getRoute(path) || path;
    history.pushState(state, '', path);
    handleRoute(path);
}

export function getRoute(routeName) {
    const { path } = routes.find(route => route.name === routeName) || {};

    if (path) {
        let regex = path;
        if (typeof (path) === 'string') {
            regex = new RegExp(path);
        }
        return new RandExp(regex).gen();
    }

    return null;
}