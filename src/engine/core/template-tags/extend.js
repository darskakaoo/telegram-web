import { render } from '../../template';

export default async function parseExtend(templateString, context, templateIsString, removeBlocks) {
    const extendsRegex = /\{\%\s*extends\s+(?<parentName>[\w\/]+)\s*\%\}/;
    const parentMatch = templateString.match(extendsRegex);
    let renderedParent;

    if (parentMatch) {
        const blockRegex = /\{\%\s*block\s+(?<blockName>\w*?)\s*\%\}(?<blockBody>[\s\S]*?)\{\%\s*endblock\s*\%\}/;

        renderedParent = await render(parentMatch.groups.parentName, context, null, templateIsString, false);

        let blockMatch;
        do {
            blockMatch = templateString.match(blockRegex);

            if (blockMatch) {
                const { blockName, blockBody } = blockMatch.groups;

                const parentBlockRegex = new RegExp(`\\{\\%\\s*block\\s+${blockName}\\s*\\%\\}(?<superBlockBody>[\\s\\S]*?)\\{\\%\\s*endblock\\s*\\%\\}`);
                const parentBlock = renderedParent.match(parentBlockRegex);

                if (!parentBlock) {
                    throw new Error(`You have extended a block that doesn\'t exist... idiot: ${blockName}`);
                }

                const newContext = {
                    ...context,
                    super: parentBlock.groups.superBlockBody
                };

                const result = await render(blockBody, newContext, null, true);

                renderedParent = renderedParent.replace(parentBlockRegex, result);

                templateString = templateString.replace(blockRegex, '');
            }
        } while (blockMatch);

        templateString = renderedParent;
        templateString = templateString.replace(/\{\%\s*block\s+\w*?\s*\%\}([\s\S]*?)\{\%\s*endblock\s*\%\}/g, (_, capture) => {
            return capture.trim();
        });
    } else if (removeBlocks) {
        templateString = templateString.replace(/\{\%\s*block\s+\w*?\s*\%\}([\s\S]*?)\{\%\s*endblock\s*\%\}/g, (_, capture) => {
            return capture.trim();
        });
    }

    return templateString;
}