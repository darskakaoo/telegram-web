import { render } from '../../template';

class Statement {
    addToBody(part) {
        return;
    }

    eval() {
        return '';
    }
}

class ForStatement extends Statement {
    constructor() {
        super();

        this.variable = '';
        this.range = [];
        this.body = '';
        this.endKeyword = 'for';
    }

    addToBody(part) {
        this.body += `\n${part}`;
    }

    async eval(context) {
        let result = '';
        for (const value of context[this.range]) {
            const newContext = {
                ...context,
                [this.variable]: value
            };

            result += await render(this.body, newContext, null, true);
        }

        return result;
    }
}

const ifParsingStates = {
    ifBody: 0,
    elseBody: 1,
    finished: 2
};

class IfStatement extends Statement {
    constructor() {
        super();

        this.ifCondition = '';
        this.ifBody = '';
        this.elseBody = '';
        this.parsingState = ifParsingStates.ifBody;
        this.endKeyword = 'if';
    }

    addToBody(part) {
        if (this.parsingState === ifParsingStates.ifBody) {
            this.ifBody += `\n${part}`;
        } else {
            this.elseBody += `\n${part}`;
        }
    }

    eval() {
        if (eval(this.ifCondition)) {
            return this.ifBody;
        } else {
            return this.elseBody;
        }
    }
}

function getLiteralValue(value) {
    if (typeof (value) === 'string') {
        return `'${value}'`;
    } else {
        return value;
    }
}

function applyContext(string, context, parent) {
    for (const variableName in context) {
        const value = context[variableName];
        let name;

        if (parent) {
            name = `${parent}.${variableName}`;
        } else {
            name = variableName;
        }

        if (value instanceof Array) {
            const lengthRegex = new RegExp(`\\b${name}\\.length\\b`, 'g');
            string = string.replace(lengthRegex, value.length);

            for (let i = 0; i < value.length; i++) {
                const variableRegex = new RegExp(`\\b${name}\\.${i}\\b`, 'g');

                string = string.replace(variableRegex, getLiteralValue(value[i]));
            }
        }
        else if (typeof (value) === 'object') {
            string = applyContext(string, value, name);
        } else {
            const variableRegex = new RegExp(`\\b${name}\\b`, 'g');

            string = string.replace(variableRegex, getLiteralValue(value));
        }

    }

    return string;
}

export default async function parseControlFlow(templateString, context) {
    templateString = templateString.replace(/\{\%/g, '\n{%').replace(/\%\}/g, '%}\n');

    const stack = [];
    const parts = templateString.split('\n');

    let currentStatement = null;
    let ignoreTags = false;
    let result = '';

    for (const part of parts) {
        if ((ignoreTags && part.indexOf('endfor') === -1) || part.indexOf('{%') === -1) {
            if (currentStatement) {
                currentStatement.addToBody(part);
            } else {
                result += `\n${part}`;
            }
        } else {
            if (currentStatement && part.indexOf(`end${currentStatement.endKeyword}`) !== -1) {
                // tag has ended
                ignoreTags = false;
                const evaluated = await currentStatement.eval(context);

                if (stack.length > 0) {
                    currentStatement = stack.pop();
                    currentStatement.addToBody(evaluated);
                } else {
                    currentStatement = null;
                    result += `\n${evaluated}`;
                }
            } else if (part.indexOf('if') !== -1) {
                // new if coming
                if (currentStatement) {
                    stack.push(currentStatement);
                }

                currentStatement = new IfStatement();
                const { condition } = part.match(/\{\%\s*if\s+(?<condition>.*?)\s*\%\}/).groups;
                currentStatement.ifCondition = applyContext(condition, context);
            } else if (part.indexOf('else') !== -1) {
                // else of if coming
                currentStatement.parsingState = ifParsingStates.elseBody;
            } else if (part.indexOf('for') !== -1) {
                // new for coming
                if (currentStatement) {
                    stack.push(currentStatement);
                }

                currentStatement = new ForStatement();
                ignoreTags = true;

                const { variable, range } = part.match(/\{\%\s*for\s+(?<variable>\w+?)\s+of\s+(?<range>\w+?)\s*\%\}/).groups;
                currentStatement.variable = variable;
                currentStatement.range = range;
            } else {
                result += `\n${part}`;
            }
        }
    }

    return result;
}