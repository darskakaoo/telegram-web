import { render } from '../../template';

const helperDiv = document.createElement('div');

function parseElementFromString(elementString) {
    helperDiv.innerHTML = elementString;

    return helperDiv.childNodes[0];
}

function parseTemplateInclude(templateElementString, context) {
    const parsedElement = parseElementFromString(templateElementString);

    const templateName = parsedElement.tagName.toLowerCase().replace(/\./g, '/');

    const newContext = {
        ...context,
    };

    for (const attr of Array.from(parsedElement.attributes)) {
        let value;

        if (attr.value.startsWith('{') && attr.value.endsWith('}')) {
            value = JSON.parse(attr.value.replace(/\'/g, '"'));
        } else {
            value = attr.value;
        }

        newContext[attr.name] = value;
    }

    return render(templateName, newContext, null);
}

export default async function parseIncludes(templateString, context) {
    templateString = await templateString.asyncReplace(
        /\{\%\s*include\s+([\w\/]+?)\s*\%\}/g,
        async (_, capture) => {
            const rendered = await render(capture, context);

            return rendered;
        },
    );

    const includedTemplates = templateString.match(/(<[A-Z][\w\.]+[\s\S]+\/>)/g);

    if (includedTemplates && includedTemplates.length > 0) {
        for (const template of includedTemplates) {
            templateString = templateString.replace(template, await parseTemplateInclude(template, context));
        }
    }

    return templateString;
}