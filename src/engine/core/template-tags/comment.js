export default function removeComments(templateString) {
    return templateString.replace(/\{\#[\s\S]*?\#\}/g, '');
}