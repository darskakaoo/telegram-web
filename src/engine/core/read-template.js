async function fetchTemplate(templateName) {
    return new Promise((resolve, reject) => {
        const templateAddress = `/templates/${templateName}.chtml`;

        const rawFile = new XMLHttpRequest();
        rawFile.open('GET', templateAddress, false);

        rawFile.onreadystatechange = () => {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status === 0) {
                    const allText = rawFile.responseText;
                    resolve(allText);
                } else {
                    reject();
                }
            }
        };

        rawFile.send(null);
    });
}


export default async function readTemplate(template, templateIsString) {
    let templateString;

    if (templateIsString) {
        templateString = template;
    } else {
        templateString = await fetchTemplate(template);
    }

    return templateString;
}