function getValueFromContext(valueSelector, context) {
    try {
        const parts = valueSelector.split('.');
        let value = context;

        for (const part of parts) {
            value = value[part];
        }

        return value;
    } catch {
        return null;
    }
}

export default function parseVariables(templateString, context) {
    const variableValueRegex = /\{\{\s*(\w[\w\.]*)\s*\}\}/g;

    templateString = templateString.replace(
        variableValueRegex,
        (_, capture) => {
            const referencedValue = getValueFromContext(capture, context);
            if (referencedValue) {
                return referencedValue;
            } else {
                return `{{ ${capture} }}`;
            }
        },
    );

    const variableRestSpreadRegex = /\{\{\s*\.\.\.(\w[\w\.]*)\s*\}\}/g;

    templateString = templateString.replace(
        variableRestSpreadRegex,
        (_, capture) => {
            const referencedValue = getValueFromContext(capture, context);
            if (referencedValue && typeof (referencedValue) === 'object') {
                return Object.entries(referencedValue).map(([key, value]) => {
                    if (typeof (value) === 'object') {
                        value = JSON.stringify(value).replace(/\"/g, "'");
                    }

                    return `${key}="${value}"`;
                }).join(' ');
            } else {
                return `{{ ...${capture} }}`;
            }
        },
    );

    return templateString;
}