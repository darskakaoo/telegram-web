import telegram from './telegram';
import User from '../models/user';

export default {
    login: async function ({ phone, api_id, api_hash }) {
        const { phone_code_hash } = await telegram('auth.sendCode', {
            phone_number: phone,
            api_id,
            api_hash,
            settings: {
                _: 'codeSettings',
                current_number: false
            }
        });

        return {
            phone_code_hash,
            phone_number: phone
        };
    },
    verifyCode: async function ({ phone_number, phone_code_hash, code }) {

        const result = await telegram('auth.signIn', {
            phone_number,
            phone_code_hash,
            phone_code: code
        });

        let nextStep;
        if (result._ === 'auth.authorizationSignUpRequired') {
            nextStep = 'signup';
        } else {
            const { user } = result;

            User.selfId = user.id;
            new User(user);

            nextStep = 'messages';

        }

        return {
            nextStep,
        };

    },
    signup: async function ({ phone_number, phone_code_hash, first_name, last_name }) {
        console.log(phone_number, phone_code_hash, first_name, last_name);

        const { user } = await telegram('auth.signUp', {
            phone_number,
            phone_code_hash,
            first_name,
            last_name
        });

        console.log('movafagh shodam');

        User.selfId = user.id;
        new User(user);


        return {};

    }
};