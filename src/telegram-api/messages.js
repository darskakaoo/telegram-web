import formatDate from '../utils/date';
import telegram from './telegram';
import User from '../models/user';
import Dialog from '../models/dialog';
import Message from '../models/message';
import Chat from '../models/chat';

export default {
    getDialogs: async function () {
        const { users, messages, dialogs, chats } = await telegram('messages.getDialogs');

        users.forEach((user) => {
            new User(user);
        });

        messages.forEach((message) => {
            new Message(message);
        });

        dialogs.forEach((dialog) => {
            new Dialog(dialog);
        });

        chats.forEach((chat) => {
            new Chat(chat);
        });

        return {
            messages: await Dialog.previews
        };
    },
    getHistory: async function ({ peer }) {
        let { messages, users } = await telegram('messages.getHistory', {
            peer
        });

        messages = messages.map(message => {
            return new Message(message);
        });

        users.forEach(user => {
            new User(user);
        });

        return {
            messages: await Promise.all(messages.reverse().map(message => message.chatInfo))
        }
    },
    readHistory: async function ({ peer }) {
        await telegram('messages.readHistory', {
            peer
        });
    },
    sendMessage: async function ({ peer, message, randomId }) {
        const { id, date } = await telegram('messages.sendMessage', {
            peer,
            message,
            random_id: randomId
        });

        const messageModel = new Message({
            date,
            from_id: await User.selfId,
            message,
            id,
            to_id: peer,
            out: true,
        });

        return {
            message: await messageModel.chatInfo
        };
    }
};

