import User from '../models/user';
import Chat from '../models/chat';

// Telegram API calls

export { default as auth } from './auth';
export { default as messages } from './messages';
export { default as upload } from './upload';
export { default as account } from './account';

function createPeer(peerType, peerId, accessHash) {
    let peer = {
        _: `inputPeer${peerType.charAt(0).toUpperCase()}${peerType.substring(1)}`,
        [`${peerType}_id`]: peerId
    };

    if (peerType !== 'chat') {
        peer.access_hash = accessHash;
    }

    return peer;
}

async function findPeer(peerId) {
    const probableUser = await User.find(peerId);

    if (probableUser) {
        return probableUser;
    } else {
        return await Chat.find(peerId);
    }
}

window.createPeer = createPeer;
window.findPeer = findPeer;