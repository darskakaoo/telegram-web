import telegram from './telegram';

const limit = 1024 * 1024; // 1 MB

export default {
    getUserPhoto: async function ({ local_id, volume_id, peer }) {
        let result = new Uint8Array();
        let bytes = new Uint8Array();
        let offset = 0;

        do {
            const response = await telegram('upload.getFile', {
                limit,
                offset,
                location: {
                    volume_id,
                    local_id,
                    peer,
                    _: 'inputPeerPhotoFileLocation'
                }
            });

            bytes = response.bytes;

            result = new Uint8Array([...result, ...bytes]);
            offset += limit;
        } while (bytes.length === limit);

        return result;
    },
};