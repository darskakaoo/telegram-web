import telegram from './telegram';

export default {
    updateStatus: async function ({ offline }) {
        await telegram('account.updateStatus', {
            offline
        });
    }
};