import './css/main.scss';
import './utils/api';
import './utils/image';
import './routes';
import './utils/initializationRunnables';

export { setRoutes, handleRoute } from './engine/router';
export { render } from './engine/template';
