import Model from './base';
import localForage from '../utils/storage';

export default class User extends Model {
    constructor({ access_hash, contact, first_name, last_name, id, phone, photo, status, username, verified, support }, createOrUpdate = true) {
        super();

        // access hash
        this.accessHash = access_hash;

        // is contact
        if (contact) {
            this.contact = true;
        } else {
            this.contact = false;
        }

        // name
        this.firstName = first_name || '';
        this.lastName = last_name || '';

        // verified and support
        if (verified) {
            this.verified = true;
        } else {
            this.verified = false;
        }

        if (support) {
            this.support = true;
        } else {
            this.support = false;
        }

        // username
        if (username) {
            this.username = username;
        } else {
            this.username = '';
        }

        // id
        this.id = id;

        // phone number
        this.phoneNumber = phone;

        // photo
        if (!photo || photo._ === 'userProfilePhotoEmpty') {
            this.photo = '';
        } else {
            this.photo = {
                dcId: photo.dc_id,
                photoId: photo.photo_id,
                smallPhoto: photo.photo_small,
                bigPhoto: photo.photo_big
            };
        }

        // status
        if (!status || status._ === 'userStatusEmpty') {
            this.status = null;
        }
        else {
            switch (status._) {
                case 'userStatusOnline': {
                    this.status = {
                        status: 'online',
                        expires: status.expires * 1000
                    };
                    break;
                }
                case 'userStatusOffline': {
                    this.status = {
                        status: 'offline',
                        lastSeen: status.was_online * 1000
                    };
                    break;
                }
                case 'userStatusRecently': {
                    this.status = {
                        status: 'Last seen recently'
                    };
                    break;
                }
                case 'userStatusLastWeek': {
                    this.status = {
                        status: 'Last seen within a week'
                    };
                    break;
                }
                case 'userStatusLastMonth': {
                    this.status = {
                        status: 'Last seen a long time ago'
                    };
                    break;
                }
                default: {
                    this.status = null;
                    break;
                }
            }
        }

        // update storage
        if (createOrUpdate) {
            User.createOrUpdate(this);
        }
    }

    get name() {
        let result = this.firstName;

        if (this.lastName) {
            result += ` ${this.lastName}`;
        }

        return result;
    }

    static get selfId() {
        return new Promise(async (resolve) => {
            resolve(await localForage.get('self-id'));
        });
    }

    static set selfId(id) {
        localForage.set('self-id', id);
    }
}