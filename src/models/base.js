import localForage from '../utils/storage';

export default class Model {
    static createKey(id) {
        return `${this.name.toLowerCase()}-${id}`;
    }

    get key() {
        return this.constructor.createKey(this.id);
    }

    static find(id) {
        const classConstructor = this;

        return new Promise(async (resolve) => {
            const storageObject = await localForage.get(this.createKey(id));

            if (storageObject) {
                const newInstance = new classConstructor({}, false);
                this.createInstanceFromStorage(newInstance, storageObject);
                resolve(newInstance);
            } else {
                resolve(null);
            }
        });
    }

    static createOrUpdate(newInstance) {
        localForage.set(newInstance.key, newInstance);
    }

    static createInstanceFromStorage(instance, storageObject) {
        for (let key in storageObject) {
            instance[key] = storageObject[key];
        }
    }

    static get all() {
        const classNameRegex = new RegExp(`${this.name.toLowerCase()}-`);
        const classConstructor = this;

        return new Promise((resolve) => {
            localForage.keys().then(keys => {
                const relatedKeys = keys.filter(key => key.match(classNameRegex) !== null);

                Promise.all(relatedKeys.map(localForage.get)).then(result => {
                    resolve(
                        result.map(storageObject => {
                            const newInstance = new classConstructor({}, false);
                            this.createInstanceFromStorage(newInstance, storageObject);

                            return newInstance;
                        })
                    );
                });
            });
        });
    }
}