import Model from './base';

export default class Chat extends Model {
    constructor({ id, date, photo, title, verified, access_hash, participants_count, _ }, createOrUpdate = true) {
        super();

        // handle type, it's either chat (group) or channel
        this.type = _;

        // id
        this.id = id;

        // date
        if (date) {
            this.date = date * 1000;
        }

        // photo
        if (!photo || photo._ === 'chatPhotoEmpty') {
            this.photo = '';
        } else {
            this.photo = {
                smallPhoto: photo.photo_small,
                bigPhoto: photo.photo_big,
                dcId: photo.dc_id
            };
        }

        // title
        this.title = title;

        // verified
        if (verified) {
            this.verified = true;
        } else {
            this.verified = false;
        }

        // members count
        if (participants_count) {
            this.memberCount = participants_count;
        } else {
            this.memberCount = 0;
        }

        this.accessHash = access_hash;

        // update storage
        if (createOrUpdate) {
            Chat.createOrUpdate(this);
        }
    }
}