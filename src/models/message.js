import Model from './base';
import formatDate from '../utils/date';
import User from './user';

export default class Message extends Model {
    static messagePreviewLength = 30;

    constructor({
        date,
        from_id,
        message,
        id,
        to_id,
        out,
        silent,
        mentioned,
        fwd_from,
        media,
        views,
        edit_date,
        post
    }, createOrUpdate = true) {
        super();

        // date
        if (date) {
            this.date = date * 1000;
        }

        // from id
        this.fromId = from_id;

        // to id
        if (to_id) {
            this.peerType = to_id._.substring(4).toLowerCase(); // remove the word peer
            this.peerId = to_id[`${this.peerType}_id`];
        }

        // message
        if (message) {
            this.message = {
                type: 'text',
                message
            };
        } else {
            if (media) {
                this.message = {
                    type: 'media',
                    // TODO use media
                };
            } else {
                this.message = '';
            }
        }

        // id
        this.id = id;

        // out
        if (out) {
            this.out = true;
        } else {
            this.out = false;
        }

        // muted
        if (silent) {
            this.muted = true;
        } else {
            this.muted = false;
        }

        // forwarded
        if (fwd_from) {
            // TODO
        }

        // channel post info
        if (post) {
            this.post = true;
            if (views) {
                this.views = views;
            } else {
                this.views = null;
            }
        } else {
            this.post = this.views = null;
        }

        // edited
        if (edit_date) {
            this.editDate = edit_date * 1000;
        } else {
            this.editDate = null;
        }

        // mentioned
        if (mentioned) {
            this.mentioned = true;
        } else {
            this.mentioned = false;
        }

        // update storage
        if (createOrUpdate) {
            Message.createOrUpdate(this);
        }
    }

    static createPreview(text) {
        return text.substring(0, Message.messagePreviewLength)
            + (text.length > Message.messagePreviewLength ? '...' : '');
    }

    get messagePreview() {
        if (this.message.type === 'text') {
            return Message.createPreview(this.message.message);
        } else {
            return 'some file...';
        }
    }

    get chatInfo() {
        return new Promise(async (resolve) => {
            const result = {
                id: this.id,
                fromId: this.fromId,
                date: null,
                self: null,
                message: null,
                supported: true,
            };

            let date;
            let edited = '';
            if (this.editDate) {
                date = new Date(this.editDate);
                edited = 'edited ';
            } else {
                date = new Date(this.date);
            }

            result.date = `${edited}${formatDate(date, 'HH:MM')}`;

            result.self = this.fromId === await User.selfId ? 'self' : ' ';

            if (this.message.type === 'text') {
                result.message = this.message.message;
            } else {
                result.supported = false;
            }

            resolve(result);
        });
    }
}