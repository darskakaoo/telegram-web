import formatDate from '../utils/date';
import User from './user';
import Message from './message';
import Chat from './chat';
import Model from './base';

export default class Dialog extends Model {
    constructor({ peer, draft, pinned, unread_count, unread_mentions_count, top_message }, createOrUpdate = true) {
        super();

        // peer
        if (peer) {
            this.peerType = peer._.substring(4).toLowerCase(); // remove the word peer
            this.peerId = peer[`${this.peerType}_id`];
        }

        // draft
        if (draft && draft._ !== 'draftMessageEmpty') {
            this.draft = {
                message: draft.message,
                date: draft.date * 1000
            };
        } else {
            this.draft = null;
        }

        // pinned
        if (pinned) {
            this.pinned = true;
        } else {
            this.pinned = false;
        }

        // unread and mentions
        if (unread_count) {
            this.unread = unread_count;
        } else {
            this.unread = 0;
        }

        if (unread_mentions_count && unread_mentions_count > 0) {
            this.hasMentions = true;
        } else {
            this.hasMentions = false;
        }

        // top message id
        if (top_message) {
            this.topMessageId = top_message;
        }

        // update storage
        if (createOrUpdate) {
            Dialog.createOrUpdate(this);
        }
    }

    get preview() {
        return new Promise(async (resolve) => {
            const result = {
                id: this.peerId,
                accessHash: null,
                photo: null,
                date: null,
                dateInt: null,
                title: null,
                message: null,
                pinned: this.pinned,
                unread: this.hasMentions ? '@' : this.unread,
                sender: null,
                senderColor: null,
                peer: {
                    peerType: this.peerType,
                    peerId: this.peerId
                }
            };

            if (this.draft) {
                result.message = Message.createPreview(this.draft.message);
                result.dateInt = this.draft.date;
                result.sender = 'Draft: ';
                result.senderColor = 'red';
            } else {
                const message = await Message.find(this.topMessageId);
                result.dateInt = message.date;
                result.message = message.messagePreview;

                if (message.fromId === await User.selfId) {
                    result.senderColor = 'accent';
                    result.sender = 'You: ';
                } else if (this.peerType === 'chat') {
                    const { name } = await User.find(message.fromId);
                    result.senderColor = ' ';
                    result.sender = `${name}: `;
                } else {
                    result.senderColor = ' ';
                    result.sender = '';
                }
            }

            if (this.peerType === 'user') {
                if (this.peerId === await User.selfId) {
                    const { accessHash } = await User.find(this.peerId);

                    result.accessHash = accessHash;
                    result.title = 'Saved Messages';
                    result.photo = '';
                } else {
                    const { name, photo, accessHash } = await User.find(this.peerId);
                    result.accessHash = accessHash;
                    result.title = name;
                    result.photo = photo;
                }

            } else {
                const { title, photo, accessHash } = await Chat.find(this.peerId);
                result.accessHash = accessHash;
                result.title = title;
                result.photo = photo;
            }

            const date = new Date(result.dateInt);
            const today = new Date();
            let format = '';

            if (date.toDateString() === today.toDateString()) {
                // messages from the same day (should use hour format)
                format = 'HH:MM';
            }
            else if (today.getDay() - date.getDate() < 7) {
                // messages from the same week (should use day name)
                format = 'D';
            } else {
                // messages from other time (should use dd/mm/yy format)
                format = 'dd.mm.yy'
            }
            result.date = formatDate(new Date(result.dateInt), format);

            resolve(result);
        });
    }

    static get previews() {
        return new Promise(async (resolve) => {
            const allDialogs = await Dialog.all;
            const previews = await Promise.all(allDialogs.map(dialog => dialog.preview));

            resolve({
                pinned: previews.filter(dialog => dialog.pinned),
                unpinned: previews.filter(dialog => !dialog.pinned).sort((d1, d2) => d2.dateInt - d1.dateInt)
            });
        });
    }

    get id() {
        return this.peerId;
    }
}