import { codeView, loginView, passwordView, signupView } from './views/auth';
import { chatListView } from './views/chat';
import { mainView } from './views/main';
import { setRoutes } from './engine/router';

setRoutes([
    {
        path: /^\/messages\/((?<peerId>\d+)\/)?$/,
        handler: chatListView,
        name: 'chatList'
    },
    {
        path: /^\/signup\/$/,
        handler: signupView,
        name: 'signup'
    },
    {
        path: /^\/login\/$/,
        handler: loginView,
        name: 'login'
    },
    {
        path: /^\/login\/code\/$/,
        handler: codeView,
        name: 'code'
    },
    {
        path: /^\/login\/password\/$/,
        handler: passwordView,
        name: 'password'
    },
    {
        path: /^$/,
        handler: mainView,
        name: 'main'
    },
]);