# Telegram-Web: Cocoa edition of telegram web client

  

### (This project was developed for telegram's javascript contest)

# Features
- SignIn:
	- Country code and Phone number selection
	- Code verification
	- **Note** If you have 2FA enabled on your telegram account, you will be redirected to a 2FA page, but the page does not make API calls to telegram.
- SignUp:
	- You can sign up to telegram using your phone number. You will be prompted to choose your first and last name.
	- **Note** you cannot choose a profile picture when signing up.
- Messaging:
	- Dialogs list: you can see the list of your open chats.
	- Reading Messages: when you select a dialog, your chat history will be loaded.
	- Sending Messages: You can send text messages in your chats.

  

## Authors:

  

Khashayar Mirsadeghi (mms.khashayar@gmail.com)

Mahdi Khosravi (mahdi0khosravi@gmail.com)

Shahriar Heshmat (shahriarheshmat@gmail.com)

# Deployment Notes
The project can be deployed using any static server, serving the contents of `dist` folder.
### Note:
In order for the template engine to work seamlessly, we strongly recommend you to config the static server to use `index.html` file as a fallback to any failed requests, this way the template's router will take control of the browser's navigator and render the desired views. If you don't do this, the code still works, but refreshing any page other than the `/` directory will result in a `404 Not Found` response.
This is a common pattern for serving web-applications developed with other libraries and frameworks such as ReactJS.

# Project structure

  

### Template engine

  

Since we've aimed to develop this project without using any third-party UI libraries (e.g. React), frameworks (e.g. Angular) or compilers (e.g. Elm) we've built our minimal templating engine. Templating syntax is inspired by Django templates (server-side templating). The engine also handles the browser's navigator and renders appropriate views. All related code can be found at `src/engine/`.

We implemented the following template tags:

  

```

{% include %}

{% extend %}

{% for var of range %}

{% if var === exp %}

{{ variable }}

<Template.dir /> // e.g. <Chat.message.singleMessage/>

```

  

### MT-Proto

  

To work with telegram's API, we've forked [telegram-mtproto](https://github.com/zerobias/telegram-mtproto), updated some dependencies, and rebuilt it to work with the latest **Layer 105 API**.
The code for this library is included in the `mtproto-engine` directory. Note that the build config for this library is **NOT** included in the `src/webpack.config.js` file. You have to build it manually using its own build script and place the resulting files in the `dist/bundle` and `dist/node_modules` folders.
You can do so by running `npm run rebuild:webogram`.

  

### Components and Templating

  

We've also implemented the atomic components using the web component API.

These can be found under the `src/templates/components`

The following tags are defined as WCs.

  

```

<button is="tg-button">

<tg-loading>

<tg-icon>

<tg-profile-image>

<tg-checkbox>

<tg-divider>

<tg-padding>

```

  

This pattern fits perfectly with the templating pattern, resulting in faster development. The engine enables us to progressively implement WC.

  

# 3rd party runtime dependencies

  

- tgs-player ([View on github](https://github.com/LottieFiles/lottie-player)):
	- used to show tgs animations in web.
- localForage ([View on github](https://github.com/localForage/localForage)):
	- used as an async storage for persisting large data on IndexedDB.
- RandExp ([View on github](https://github.com/fent/randexp.js)):
	- used in template engine to generate urls from views that have deterministic regex patterns.
- telegram-mtproto ([View on github](https://github.com/zerobias/telegram-mtproto)):
	- for usage explanation, see [mtproto section above](#MT-Proto)
  

# Icons

  

We used [icomoon](http://icomoon.io/) to convert svgs into font-icons.

  

# Theme

  

We used original macOS telegram's theme which generated by [themes](https://themes.contest.com/). Theoretically, we can support themes. But functionality wasn't implement because of lack of time.

  

P.S. As we're writing this document, the international web has been blocked by the Islamic Republic of Iran's government. We're using the pigeon protocol ([RFC](https://tools.ietf.org/html/rfc1149), [WIKI](https://en.wikipedia.org/wiki/IP_over_Avian_Carriers)) to submit this. We couldn't test our code for the last two days of the contest :D, hope it all works.

  

# Special thanks:

  

Soroush Nikpour ([@oraclenik](https://twitter.com/oraclenik))

[DarsKakaoo](https://darskakaoo.ir) team for supporting us. Love you guys <3!