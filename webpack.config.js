const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const publicPath = path.resolve(__dirname, 'dist');

module.exports = {
    output: {
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /templates/],
                loader: 'babel-loader'
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            hmr: true,
                        },
                    },
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            }
        ]
    },

    resolve: {
        extensions: ['*', '.js', '.scss', '.chtml']
    },

    devtool: 'inline-source-map',
    devServer: {
        port: 3000,
        hot: true,
        historyApiFallback: true
    },

    plugins: [
        new CopyPlugin([
            {
                ignore: ['*.scss'],
                from: '**/*',
                to: 'templates/',
                context: 'src/templates/'
            },
        ]),
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        new HtmlWebpackPlugin({
            minify: true,
            cache: true,
            hash: true,

            template: path.join(publicPath, 'index.html'),
            filename: 'index.html',
        })
    ]
};
